
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    const identity = x => x;
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function is_promise(value) {
        return value && typeof value === 'object' && typeof value.then === 'function';
    }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }
    function validate_store(store, name) {
        if (store != null && typeof store.subscribe !== 'function') {
            throw new Error(`'${name}' is not a store with a 'subscribe' method`);
        }
    }
    function subscribe(store, ...callbacks) {
        if (store == null) {
            return noop;
        }
        const unsub = store.subscribe(...callbacks);
        return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
    }
    function component_subscribe(component, store, callback) {
        component.$$.on_destroy.push(subscribe(store, callback));
    }
    function create_slot(definition, ctx, $$scope, fn) {
        if (definition) {
            const slot_ctx = get_slot_context(definition, ctx, $$scope, fn);
            return definition[0](slot_ctx);
        }
    }
    function get_slot_context(definition, ctx, $$scope, fn) {
        return definition[1] && fn
            ? assign($$scope.ctx.slice(), definition[1](fn(ctx)))
            : $$scope.ctx;
    }
    function get_slot_changes(definition, $$scope, dirty, fn) {
        if (definition[2] && fn) {
            const lets = definition[2](fn(dirty));
            if ($$scope.dirty === undefined) {
                return lets;
            }
            if (typeof lets === 'object') {
                const merged = [];
                const len = Math.max($$scope.dirty.length, lets.length);
                for (let i = 0; i < len; i += 1) {
                    merged[i] = $$scope.dirty[i] | lets[i];
                }
                return merged;
            }
            return $$scope.dirty | lets;
        }
        return $$scope.dirty;
    }
    function update_slot(slot, slot_definition, ctx, $$scope, dirty, get_slot_changes_fn, get_slot_context_fn) {
        const slot_changes = get_slot_changes(slot_definition, $$scope, dirty, get_slot_changes_fn);
        if (slot_changes) {
            const slot_context = get_slot_context(slot_definition, ctx, $$scope, get_slot_context_fn);
            slot.p(slot_context, slot_changes);
        }
    }
    function action_destroyer(action_result) {
        return action_result && is_function(action_result.destroy) ? action_result.destroy : noop;
    }

    const is_client = typeof window !== 'undefined';
    let now = is_client
        ? () => window.performance.now()
        : () => Date.now();
    let raf = is_client ? cb => requestAnimationFrame(cb) : noop;

    const tasks = new Set();
    function run_tasks(now) {
        tasks.forEach(task => {
            if (!task.c(now)) {
                tasks.delete(task);
                task.f();
            }
        });
        if (tasks.size !== 0)
            raf(run_tasks);
    }
    /**
     * Creates a new task that runs on each raf frame
     * until it returns a falsy value or is aborted
     */
    function loop(callback) {
        let task;
        if (tasks.size === 0)
            raf(run_tasks);
        return {
            promise: new Promise(fulfill => {
                tasks.add(task = { c: callback, f: fulfill });
            }),
            abort() {
                tasks.delete(task);
            }
        };
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_style(node, key, value, important) {
        node.style.setProperty(key, value, important ? 'important' : '');
    }
    function toggle_class(element, name, toggle) {
        element.classList[toggle ? 'add' : 'remove'](name);
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }
    class HtmlTag {
        constructor(anchor = null) {
            this.a = anchor;
            this.e = this.n = null;
        }
        m(html, target, anchor = null) {
            if (!this.e) {
                this.e = element(target.nodeName);
                this.t = target;
                this.h(html);
            }
            this.i(anchor);
        }
        h(html) {
            this.e.innerHTML = html;
            this.n = Array.from(this.e.childNodes);
        }
        i(anchor) {
            for (let i = 0; i < this.n.length; i += 1) {
                insert(this.t, this.n[i], anchor);
            }
        }
        p(html) {
            this.d();
            this.h(html);
            this.i(this.a);
        }
        d() {
            this.n.forEach(detach);
        }
    }

    const active_docs = new Set();
    let active = 0;
    // https://github.com/darkskyapp/string-hash/blob/master/index.js
    function hash(str) {
        let hash = 5381;
        let i = str.length;
        while (i--)
            hash = ((hash << 5) - hash) ^ str.charCodeAt(i);
        return hash >>> 0;
    }
    function create_rule(node, a, b, duration, delay, ease, fn, uid = 0) {
        const step = 16.666 / duration;
        let keyframes = '{\n';
        for (let p = 0; p <= 1; p += step) {
            const t = a + (b - a) * ease(p);
            keyframes += p * 100 + `%{${fn(t, 1 - t)}}\n`;
        }
        const rule = keyframes + `100% {${fn(b, 1 - b)}}\n}`;
        const name = `__svelte_${hash(rule)}_${uid}`;
        const doc = node.ownerDocument;
        active_docs.add(doc);
        const stylesheet = doc.__svelte_stylesheet || (doc.__svelte_stylesheet = doc.head.appendChild(element('style')).sheet);
        const current_rules = doc.__svelte_rules || (doc.__svelte_rules = {});
        if (!current_rules[name]) {
            current_rules[name] = true;
            stylesheet.insertRule(`@keyframes ${name} ${rule}`, stylesheet.cssRules.length);
        }
        const animation = node.style.animation || '';
        node.style.animation = `${animation ? `${animation}, ` : ''}${name} ${duration}ms linear ${delay}ms 1 both`;
        active += 1;
        return name;
    }
    function delete_rule(node, name) {
        const previous = (node.style.animation || '').split(', ');
        const next = previous.filter(name
            ? anim => anim.indexOf(name) < 0 // remove specific animation
            : anim => anim.indexOf('__svelte') === -1 // remove all Svelte animations
        );
        const deleted = previous.length - next.length;
        if (deleted) {
            node.style.animation = next.join(', ');
            active -= deleted;
            if (!active)
                clear_rules();
        }
    }
    function clear_rules() {
        raf(() => {
            if (active)
                return;
            active_docs.forEach(doc => {
                const stylesheet = doc.__svelte_stylesheet;
                let i = stylesheet.cssRules.length;
                while (i--)
                    stylesheet.deleteRule(i);
                doc.__svelte_rules = {};
            });
            active_docs.clear();
        });
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error('Function called outside component initialization');
        return current_component;
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail);
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
            }
        };
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            set_current_component(null);
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }

    let promise;
    function wait() {
        if (!promise) {
            promise = Promise.resolve();
            promise.then(() => {
                promise = null;
            });
        }
        return promise;
    }
    function dispatch(node, direction, kind) {
        node.dispatchEvent(custom_event(`${direction ? 'intro' : 'outro'}${kind}`));
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }
    const null_transition = { duration: 0 };
    function create_in_transition(node, fn, params) {
        let config = fn(node, params);
        let running = false;
        let animation_name;
        let task;
        let uid = 0;
        function cleanup() {
            if (animation_name)
                delete_rule(node, animation_name);
        }
        function go() {
            const { delay = 0, duration = 300, easing = identity, tick = noop, css } = config || null_transition;
            if (css)
                animation_name = create_rule(node, 0, 1, duration, delay, easing, css, uid++);
            tick(0, 1);
            const start_time = now() + delay;
            const end_time = start_time + duration;
            if (task)
                task.abort();
            running = true;
            add_render_callback(() => dispatch(node, true, 'start'));
            task = loop(now => {
                if (running) {
                    if (now >= end_time) {
                        tick(1, 0);
                        dispatch(node, true, 'end');
                        cleanup();
                        return running = false;
                    }
                    if (now >= start_time) {
                        const t = easing((now - start_time) / duration);
                        tick(t, 1 - t);
                    }
                }
                return running;
            });
        }
        let started = false;
        return {
            start() {
                if (started)
                    return;
                delete_rule(node);
                if (is_function(config)) {
                    config = config();
                    wait().then(go);
                }
                else {
                    go();
                }
            },
            invalidate() {
                started = false;
            },
            end() {
                if (running) {
                    cleanup();
                    running = false;
                }
            }
        };
    }
    function create_out_transition(node, fn, params) {
        let config = fn(node, params);
        let running = true;
        let animation_name;
        const group = outros;
        group.r += 1;
        function go() {
            const { delay = 0, duration = 300, easing = identity, tick = noop, css } = config || null_transition;
            if (css)
                animation_name = create_rule(node, 1, 0, duration, delay, easing, css);
            const start_time = now() + delay;
            const end_time = start_time + duration;
            add_render_callback(() => dispatch(node, false, 'start'));
            loop(now => {
                if (running) {
                    if (now >= end_time) {
                        tick(0, 1);
                        dispatch(node, false, 'end');
                        if (!--group.r) {
                            // this will result in `end()` being called,
                            // so we don't need to clean up here
                            run_all(group.c);
                        }
                        return false;
                    }
                    if (now >= start_time) {
                        const t = easing((now - start_time) / duration);
                        tick(1 - t, t);
                    }
                }
                return running;
            });
        }
        if (is_function(config)) {
            wait().then(() => {
                // @ts-ignore
                config = config();
                go();
            });
        }
        else {
            go();
        }
        return {
            end(reset) {
                if (reset && config.tick) {
                    config.tick(1, 0);
                }
                if (running) {
                    if (animation_name)
                        delete_rule(node, animation_name);
                    running = false;
                }
            }
        };
    }

    function handle_promise(promise, info) {
        const token = info.token = {};
        function update(type, index, key, value) {
            if (info.token !== token)
                return;
            info.resolved = value;
            let child_ctx = info.ctx;
            if (key !== undefined) {
                child_ctx = child_ctx.slice();
                child_ctx[key] = value;
            }
            const block = type && (info.current = type)(child_ctx);
            let needs_flush = false;
            if (info.block) {
                if (info.blocks) {
                    info.blocks.forEach((block, i) => {
                        if (i !== index && block) {
                            group_outros();
                            transition_out(block, 1, 1, () => {
                                if (info.blocks[i] === block) {
                                    info.blocks[i] = null;
                                }
                            });
                            check_outros();
                        }
                    });
                }
                else {
                    info.block.d(1);
                }
                block.c();
                transition_in(block, 1);
                block.m(info.mount(), info.anchor);
                needs_flush = true;
            }
            info.block = block;
            if (info.blocks)
                info.blocks[index] = block;
            if (needs_flush) {
                flush();
            }
        }
        if (is_promise(promise)) {
            const current_component = get_current_component();
            promise.then(value => {
                set_current_component(current_component);
                update(info.then, 1, info.value, value);
                set_current_component(null);
            }, error => {
                set_current_component(current_component);
                update(info.catch, 2, info.error, error);
                set_current_component(null);
                if (!info.hasCatch) {
                    throw error;
                }
            });
            // if we previously had a then/catch block, destroy it
            if (info.current !== info.pending) {
                update(info.pending, 0);
                return true;
            }
        }
        else {
            if (info.current !== info.then) {
                update(info.then, 1, info.value, promise);
                return true;
            }
            info.resolved = promise;
        }
    }
    function update_await_block_branch(info, ctx, dirty) {
        const child_ctx = ctx.slice();
        const { resolved } = info;
        if (info.current === info.then) {
            child_ctx[info.value] = resolved;
        }
        if (info.current === info.catch) {
            child_ctx[info.error] = resolved;
        }
        info.block.p(child_ctx, dirty);
    }

    const globals = (typeof window !== 'undefined'
        ? window
        : typeof globalThis !== 'undefined'
            ? globalThis
            : global);
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor, customElement) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        if (!customElement) {
            // onMount happens before the initial afterUpdate
            add_render_callback(() => {
                const new_on_destroy = on_mount.map(run).filter(is_function);
                if (on_destroy) {
                    on_destroy.push(...new_on_destroy);
                }
                else {
                    // Edge case - component was destroyed immediately,
                    // most likely as a result of a binding initialising
                    run_all(new_on_destroy);
                }
                component.$$.on_mount = [];
            });
        }
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            on_disconnect: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : options.context || []),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, options.props || {}, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor, options.customElement);
            flush();
        }
        set_current_component(parent_component);
    }
    /**
     * Base class for Svelte components. Used when dev=false.
     */
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set($$props) {
            if (this.$$set && !is_empty($$props)) {
                this.$$.skip_bound = true;
                this.$$set($$props);
                this.$$.skip_bound = false;
            }
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.38.2' }, detail)));
    }
    function append_dev(target, node) {
        dispatch_dev('SvelteDOMInsert', { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev('SvelteDOMInsert', { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev('SvelteDOMRemove', { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ['capture'] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev('SvelteDOMAddEventListener', { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev('SvelteDOMRemoveEventListener', { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev('SvelteDOMRemoveAttribute', { node, attribute });
        else
            dispatch_dev('SvelteDOMSetAttribute', { node, attribute, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.wholeText === data)
            return;
        dispatch_dev('SvelteDOMSetData', { node: text, data });
        text.data = data;
    }
    function validate_each_argument(arg) {
        if (typeof arg !== 'string' && !(arg && typeof arg === 'object' && 'length' in arg)) {
            let msg = '{#each} only iterates over array-like objects.';
            if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
                msg += ' You can use a spread to convert this iterable into an array.';
            }
            throw new Error(msg);
        }
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    /**
     * Base class for Svelte components with some minor dev-enhancements. Used when dev=true.
     */
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error("'target' is a required option");
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn('Component was already destroyed'); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    /* src/components/Tabs.svelte generated by Svelte v3.38.2 */
    const file$b = "src/components/Tabs.svelte";

    function get_each_context$3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[4] = list[i];
    	return child_ctx;
    }

    // (9:2) {#each items as item}
    function create_each_block$3(ctx) {
    	let li;
    	let div;
    	let t0_value = /*item*/ ctx[4] + "";
    	let t0;
    	let t1;
    	let mounted;
    	let dispose;

    	function click_handler() {
    		return /*click_handler*/ ctx[3](/*item*/ ctx[4]);
    	}

    	const block = {
    		c: function create() {
    			li = element("li");
    			div = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			attr_dev(div, "class", "svelte-1vwd8s9");
    			toggle_class(div, "active", /*item*/ ctx[4] === /*activeItem*/ ctx[1]);
    			add_location(div, file$b, 10, 4, 274);
    			attr_dev(li, "class", "svelte-1vwd8s9");
    			add_location(li, file$b, 9, 3, 220);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, li, anchor);
    			append_dev(li, div);
    			append_dev(div, t0);
    			append_dev(li, t1);

    			if (!mounted) {
    				dispose = listen_dev(li, "click", click_handler, false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*items*/ 1 && t0_value !== (t0_value = /*item*/ ctx[4] + "")) set_data_dev(t0, t0_value);

    			if (dirty & /*items, activeItem*/ 3) {
    				toggle_class(div, "active", /*item*/ ctx[4] === /*activeItem*/ ctx[1]);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(li);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$3.name,
    		type: "each",
    		source: "(9:2) {#each items as item}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$b(ctx) {
    	let div;
    	let ul;
    	let each_value = /*items*/ ctx[0];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$3(get_each_context$3(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			div = element("div");
    			ul = element("ul");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(ul, "class", "svelte-1vwd8s9");
    			add_location(ul, file$b, 7, 1, 188);
    			attr_dev(div, "class", "tabs svelte-1vwd8s9");
    			add_location(div, file$b, 6, 0, 168);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, ul);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(ul, null);
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*dispatch, items, activeItem*/ 7) {
    				each_value = /*items*/ ctx[0];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$3(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$3(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(ul, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$b.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$b($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("Tabs", slots, []);
    	const dispatch = createEventDispatcher();
    	let { items } = $$props;
    	let { activeItem } = $$props;
    	const writable_props = ["items", "activeItem"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Tabs> was created with unknown prop '${key}'`);
    	});

    	const click_handler = item => dispatch("tabChange", item);

    	$$self.$$set = $$props => {
    		if ("items" in $$props) $$invalidate(0, items = $$props.items);
    		if ("activeItem" in $$props) $$invalidate(1, activeItem = $$props.activeItem);
    	};

    	$$self.$capture_state = () => ({
    		createEventDispatcher,
    		dispatch,
    		items,
    		activeItem
    	});

    	$$self.$inject_state = $$props => {
    		if ("items" in $$props) $$invalidate(0, items = $$props.items);
    		if ("activeItem" in $$props) $$invalidate(1, activeItem = $$props.activeItem);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [items, activeItem, dispatch, click_handler];
    }

    class Tabs extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$b, create_fragment$b, safe_not_equal, { items: 0, activeItem: 1 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Tabs",
    			options,
    			id: create_fragment$b.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*items*/ ctx[0] === undefined && !("items" in props)) {
    			console.warn("<Tabs> was created without expected prop 'items'");
    		}

    		if (/*activeItem*/ ctx[1] === undefined && !("activeItem" in props)) {
    			console.warn("<Tabs> was created without expected prop 'activeItem'");
    		}
    	}

    	get items() {
    		throw new Error("<Tabs>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set items(value) {
    		throw new Error("<Tabs>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get activeItem() {
    		throw new Error("<Tabs>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set activeItem(value) {
    		throw new Error("<Tabs>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    const subscriber_queue = [];
    /**
     * Create a `Writable` store that allows both updating and reading by subscription.
     * @param {*=}value initial value
     * @param {StartStopNotifier=}start start and stop notifications for subscriptions
     */
    function writable(value, start = noop) {
        let stop;
        const subscribers = [];
        function set(new_value) {
            if (safe_not_equal(value, new_value)) {
                value = new_value;
                if (stop) { // store is ready
                    const run_queue = !subscriber_queue.length;
                    for (let i = 0; i < subscribers.length; i += 1) {
                        const s = subscribers[i];
                        s[1]();
                        subscriber_queue.push(s, value);
                    }
                    if (run_queue) {
                        for (let i = 0; i < subscriber_queue.length; i += 2) {
                            subscriber_queue[i][0](subscriber_queue[i + 1]);
                        }
                        subscriber_queue.length = 0;
                    }
                }
            }
        }
        function update(fn) {
            set(fn(value));
        }
        function subscribe(run, invalidate = noop) {
            const subscriber = [run, invalidate];
            subscribers.push(subscriber);
            if (subscribers.length === 1) {
                stop = start(set) || noop;
            }
            run(value);
            return () => {
                const index = subscribers.indexOf(subscriber);
                if (index !== -1) {
                    subscribers.splice(index, 1);
                }
                if (subscribers.length === 0) {
                    stop();
                    stop = null;
                }
            };
        }
        return { set, update, subscribe };
    }

    var TempUnit;
    (function (TempUnit) {
        TempUnit[TempUnit["Kelvin"] = 0] = "Kelvin";
        TempUnit[TempUnit["Celsius"] = 1] = "Celsius";
        TempUnit[TempUnit["Fahrenheit"] = 2] = "Fahrenheit";
    })(TempUnit || (TempUnit = {}));
    var SpeedUnit;
    (function (SpeedUnit) {
        SpeedUnit[SpeedUnit["KpH"] = 0] = "KpH";
        SpeedUnit[SpeedUnit["MpH"] = 1] = "MpH";
    })(SpeedUnit || (SpeedUnit = {}));

    const apiKey = "fe9bfa061883b7ebbb0b034fa3305714";
    const celsiusSymbol = "&#8451;";
    const fahrenheitSymbol = "&#8457;";
    const kelvinSymbol = "&#8490;";
    const KpHSymbol = "km/h";
    const MpHSymbol = "Mph";
    // Calgary
    //const callatitude: number = 51.1250295;
    //const callongitude: number = -114.2507148;
    //Nicola BC
    // const latitude: number = 49.2580;
    // const longitude: number = -122.7440;
    let latitude = 49.2580;
    let longitude = -122.7440;
    let oneCallURL = `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&appid=${apiKey}`;
    const selectedTempUnit = writable(TempUnit.Celsius);
    const selectedSpeedUnit = writable(SpeedUnit.KpH);
    function changeCoords(newLatitude, newLongitude) {
        latitude = newLatitude;
        longitude = newLongitude;
        // don't like this: since not a .svelte file the latitude/longitude values can't be used reactively so have to mainly update...
        oneCallURL = `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&appid=${apiKey}`;
    }
    // default value from api is in Kelvin so value passed in will always be in Kelvin
    function convertTemp(value, unit) {
        let result;
        switch (unit) {
            case TempUnit.Kelvin:
                result = value;
                break;
            case TempUnit.Celsius:
                result = value - 273.15;
                break;
            case TempUnit.Fahrenheit:
                result = (value - 273.15) * (9 / 5) + 32;
                break;
        }
        return result.toFixed(1);
    }
    // passed value is always in m/s
    function convertSpeed(value, unit) {
        let result;
        switch (unit) {
            case SpeedUnit.KpH:
                result = value * 3.6;
                break;
            case SpeedUnit.MpH:
                result = value * 2.237;
                break;
        }
        return result.toFixed(0);
    }
    function getTempSymbol(unit) {
        switch (unit) {
            case TempUnit.Kelvin:
                return kelvinSymbol;
            case TempUnit.Celsius:
                return celsiusSymbol;
            case TempUnit.Fahrenheit:
                return fahrenheitSymbol;
        }
    }
    function getSpeedSymbol(unit) {
        switch (unit) {
            case SpeedUnit.KpH:
                return KpHSymbol;
            case SpeedUnit.MpH:
                return MpHSymbol;
        }
    }
    function booleanStore(initial) {
        const isOpen = writable(initial);
        const { set, update } = isOpen;
        return {
            isOpen,
            open: () => set(true),
            close: () => set(false),
            toggle: () => update((n) => !n),
        };
    }

    /* src/components/WeatherWarning.svelte generated by Svelte v3.38.2 */

    const file$a = "src/components/WeatherWarning.svelte";

    // (10:0) {#if visible}
    function create_if_block$5(ctx) {
    	let div;
    	let p;
    	let raw_value = /*alert*/ ctx[0].description + "";

    	const block = {
    		c: function create() {
    			div = element("div");
    			p = element("p");
    			attr_dev(p, "class", "warningDescription svelte-zklrqn");
    			add_location(p, file$a, 11, 2, 309);
    			attr_dev(div, "class", "warning svelte-zklrqn");
    			add_location(div, file$a, 10, 1, 285);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, p);
    			p.innerHTML = raw_value;
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*alert*/ 1 && raw_value !== (raw_value = /*alert*/ ctx[0].description + "")) p.innerHTML = raw_value;		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$5.name,
    		type: "if",
    		source: "(10:0) {#if visible}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$a(ctx) {
    	let button;
    	let t0_value = /*alert*/ ctx[0].event + "";
    	let t0;
    	let t1;
    	let t2;
    	let if_block_anchor;
    	let mounted;
    	let dispose;
    	let if_block = /*visible*/ ctx[1] && create_if_block$5(ctx);

    	const block = {
    		c: function create() {
    			button = element("button");
    			t0 = text(t0_value);
    			t1 = text(" warning");
    			t2 = space();
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    			attr_dev(button, "type", "button");
    			attr_dev(button, "class", "collapsible svelte-zklrqn");
    			toggle_class(button, "closed", !/*visible*/ ctx[1]);
    			toggle_class(button, "open", /*visible*/ ctx[1]);
    			add_location(button, file$a, 8, 0, 129);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, button, anchor);
    			append_dev(button, t0);
    			append_dev(button, t1);
    			insert_dev(target, t2, anchor);
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);

    			if (!mounted) {
    				dispose = listen_dev(button, "click", /*handleClick*/ ctx[2], false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*alert*/ 1 && t0_value !== (t0_value = /*alert*/ ctx[0].event + "")) set_data_dev(t0, t0_value);

    			if (dirty & /*visible*/ 2) {
    				toggle_class(button, "closed", !/*visible*/ ctx[1]);
    			}

    			if (dirty & /*visible*/ 2) {
    				toggle_class(button, "open", /*visible*/ ctx[1]);
    			}

    			if (/*visible*/ ctx[1]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block$5(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(button);
    			if (detaching) detach_dev(t2);
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$a.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$a($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("WeatherWarning", slots, []);
    	
    	let { alert } = $$props;
    	let visible = false;

    	function handleClick() {
    		$$invalidate(1, visible = !visible);
    	}

    	const writable_props = ["alert"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<WeatherWarning> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ("alert" in $$props) $$invalidate(0, alert = $$props.alert);
    	};

    	$$self.$capture_state = () => ({ alert, visible, handleClick });

    	$$self.$inject_state = $$props => {
    		if ("alert" in $$props) $$invalidate(0, alert = $$props.alert);
    		if ("visible" in $$props) $$invalidate(1, visible = $$props.visible);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [alert, visible, handleClick];
    }

    class WeatherWarning extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$a, create_fragment$a, safe_not_equal, { alert: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "WeatherWarning",
    			options,
    			id: create_fragment$a.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*alert*/ ctx[0] === undefined && !("alert" in props)) {
    			console.warn("<WeatherWarning> was created without expected prop 'alert'");
    		}
    	}

    	get alert() {
    		throw new Error("<WeatherWarning>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set alert(value) {
    		throw new Error("<WeatherWarning>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/forecasts/TodayForecast.svelte generated by Svelte v3.38.2 */

    const { console: console_1 } = globals;
    const file$9 = "src/components/forecasts/TodayForecast.svelte";

    function get_each_context$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[3] = list[i];
    	return child_ctx;
    }

    function get_each_context_1$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[6] = list[i];
    	return child_ctx;
    }

    // (17:1) {#if weatherData.alerts}
    function create_if_block_1$2(ctx) {
    	let div;
    	let current;
    	let each_value_1 = /*weatherData*/ ctx[0].alerts;
    	validate_each_argument(each_value_1);
    	let each_blocks = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks[i] = create_each_block_1$1(get_each_context_1$1(ctx, each_value_1, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const block = {
    		c: function create() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(div, "class", "weatherWarning");
    			add_location(div, file$9, 17, 2, 499);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*weatherData*/ 1) {
    				each_value_1 = /*weatherData*/ ctx[0].alerts;
    				validate_each_argument(each_value_1);
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1$1(ctx, each_value_1, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block_1$1(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();

    				for (i = each_value_1.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value_1.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1$2.name,
    		type: "if",
    		source: "(17:1) {#if weatherData.alerts}",
    		ctx
    	});

    	return block;
    }

    // (19:3) {#each weatherData.alerts as alert}
    function create_each_block_1$1(ctx) {
    	let weatherwarning;
    	let current;

    	weatherwarning = new WeatherWarning({
    			props: { alert: /*alert*/ ctx[6] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(weatherwarning.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(weatherwarning, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const weatherwarning_changes = {};
    			if (dirty & /*weatherData*/ 1) weatherwarning_changes.alert = /*alert*/ ctx[6];
    			weatherwarning.$set(weatherwarning_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(weatherwarning.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(weatherwarning.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(weatherwarning, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_1$1.name,
    		type: "each",
    		source: "(19:3) {#each weatherData.alerts as alert}",
    		ctx
    	});

    	return block;
    }

    // (62:5) {:else}
    function create_else_block$1(ctx) {
    	let t0_value = timeConverter$1(/*hour*/ ctx[3].dt) - 12 + "";
    	let t0;
    	let t1;

    	const block = {
    		c: function create() {
    			t0 = text(t0_value);
    			t1 = text("p.m.");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t0, anchor);
    			insert_dev(target, t1, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*weatherData*/ 1 && t0_value !== (t0_value = timeConverter$1(/*hour*/ ctx[3].dt) - 12 + "")) set_data_dev(t0, t0_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(t1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$1.name,
    		type: "else",
    		source: "(62:5) {:else}",
    		ctx
    	});

    	return block;
    }

    // (60:5) {#if timeConverter(hour.dt) <= 12}
    function create_if_block$4(ctx) {
    	let t0_value = timeConverter$1(/*hour*/ ctx[3].dt) + "";
    	let t0;
    	let t1;

    	const block = {
    		c: function create() {
    			t0 = text(t0_value);
    			t1 = text("a.m.");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t0, anchor);
    			insert_dev(target, t1, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*weatherData*/ 1 && t0_value !== (t0_value = timeConverter$1(/*hour*/ ctx[3].dt) + "")) set_data_dev(t0, t0_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(t1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$4.name,
    		type: "if",
    		source: "(60:5) {#if timeConverter(hour.dt) <= 12}",
    		ctx
    	});

    	return block;
    }

    // (49:2) {#each weatherData.hourly.slice(0, 25) as hour}
    function create_each_block$2(ctx) {
    	let div3;
    	let div0;
    	let t0_value = convertTemp(/*hour*/ ctx[3].temp, /*$selectedTempUnit*/ ctx[1]) + "";
    	let t0;
    	let html_tag;
    	let raw_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "";
    	let t1;
    	let div1;
    	let img;
    	let img_src_value;
    	let img_alt_value;
    	let t2;
    	let div2;
    	let show_if;
    	let t3;

    	function select_block_type(ctx, dirty) {
    		if (show_if == null || dirty & /*weatherData*/ 1) show_if = !!(timeConverter$1(/*hour*/ ctx[3].dt) <= 12);
    		if (show_if) return create_if_block$4;
    		return create_else_block$1;
    	}

    	let current_block_type = select_block_type(ctx, -1);
    	let if_block = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div1 = element("div");
    			img = element("img");
    			t2 = space();
    			div2 = element("div");
    			if_block.c();
    			t3 = space();
    			html_tag = new HtmlTag(null);
    			add_location(div0, file$9, 50, 4, 1701);
    			if (img.src !== (img_src_value = "images/" + /*hour*/ ctx[3].weather[0].icon + ".png")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", img_alt_value = /*hour*/ ctx[3].weather[0].description);
    			add_location(img, file$9, 55, 5, 1823);
    			add_location(div1, file$9, 54, 4, 1812);
    			add_location(div2, file$9, 58, 4, 1921);
    			attr_dev(div3, "class", "hourlyTemp svelte-1ync8gy");
    			add_location(div3, file$9, 49, 3, 1672);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div0);
    			append_dev(div0, t0);
    			html_tag.m(raw_value, div0);
    			append_dev(div3, t1);
    			append_dev(div3, div1);
    			append_dev(div1, img);
    			append_dev(div3, t2);
    			append_dev(div3, div2);
    			if_block.m(div2, null);
    			append_dev(div3, t3);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*weatherData, $selectedTempUnit*/ 3 && t0_value !== (t0_value = convertTemp(/*hour*/ ctx[3].temp, /*$selectedTempUnit*/ ctx[1]) + "")) set_data_dev(t0, t0_value);
    			if (dirty & /*$selectedTempUnit*/ 2 && raw_value !== (raw_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "")) html_tag.p(raw_value);

    			if (dirty & /*weatherData*/ 1 && img.src !== (img_src_value = "images/" + /*hour*/ ctx[3].weather[0].icon + ".png")) {
    				attr_dev(img, "src", img_src_value);
    			}

    			if (dirty & /*weatherData*/ 1 && img_alt_value !== (img_alt_value = /*hour*/ ctx[3].weather[0].description)) {
    				attr_dev(img, "alt", img_alt_value);
    			}

    			if (current_block_type === (current_block_type = select_block_type(ctx, dirty)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div2, null);
    				}
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);
    			if_block.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$2.name,
    		type: "each",
    		source: "(49:2) {#each weatherData.hourly.slice(0, 25) as hour}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$9(ctx) {
    	let main;
    	let t0;
    	let div6;
    	let div0;
    	let t1;
    	let t2_value = convertTemp(/*weatherData*/ ctx[0].daily[0].temp.max, /*$selectedTempUnit*/ ctx[1]) + "";
    	let t2;
    	let html_tag;
    	let raw0_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "";
    	let html_anchor;
    	let html_tag_1;
    	let t3;
    	let t4_value = convertTemp(/*weatherData*/ ctx[0].daily[0].temp.min, /*$selectedTempUnit*/ ctx[1]) + "";
    	let t4;
    	let html_tag_2;
    	let raw2_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "";
    	let html_anchor_1;
    	let html_tag_3;
    	let t5;
    	let div1;
    	let img;
    	let img_src_value;
    	let img_alt_value;
    	let t6;
    	let div2;
    	let t7_value = /*weatherData*/ ctx[0].current.weather[0].description + "";
    	let t7;
    	let t8;
    	let div3;
    	let t10;
    	let div4;
    	let t11_value = convertTemp(/*weatherData*/ ctx[0].current.temp, /*$selectedTempUnit*/ ctx[1]) + "";
    	let t11;
    	let html_tag_4;
    	let raw4_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "";
    	let t12;
    	let div5;
    	let t13;
    	let t14_value = convertTemp(/*weatherData*/ ctx[0].current.feels_like, /*$selectedTempUnit*/ ctx[1]) + "";
    	let t14;
    	let html_tag_5;
    	let raw5_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "";
    	let t15;
    	let div7;
    	let current;
    	let if_block = /*weatherData*/ ctx[0].alerts && create_if_block_1$2(ctx);
    	let each_value = /*weatherData*/ ctx[0].hourly.slice(0, 25);
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$2(get_each_context$2(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			main = element("main");
    			if (if_block) if_block.c();
    			t0 = space();
    			div6 = element("div");
    			div0 = element("div");
    			t1 = text("Day ");
    			t2 = text(t2_value);
    			html_anchor = empty();
    			t3 = text(" Night ");
    			t4 = text(t4_value);
    			html_anchor_1 = empty();
    			t5 = space();
    			div1 = element("div");
    			img = element("img");
    			t6 = space();
    			div2 = element("div");
    			t7 = text(t7_value);
    			t8 = space();
    			div3 = element("div");
    			div3.textContent = `${/*today*/ ctx[2].toDateString()}`;
    			t10 = space();
    			div4 = element("div");
    			t11 = text(t11_value);
    			t12 = space();
    			div5 = element("div");
    			t13 = text("Feels like: ");
    			t14 = text(t14_value);
    			t15 = space();
    			div7 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			html_tag = new HtmlTag(html_anchor);
    			html_tag_1 = new HtmlTag(t3);
    			html_tag_2 = new HtmlTag(html_anchor_1);
    			html_tag_3 = new HtmlTag(null);
    			attr_dev(div0, "class", "weatherMinMax svelte-1ync8gy");
    			add_location(div0, file$9, 24, 2, 658);
    			if (img.src !== (img_src_value = "images/" + /*weatherData*/ ctx[0].current.weather[0].icon + "@2x.png")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", img_alt_value = /*weatherData*/ ctx[0].current.weather[0].description);
    			add_location(img, file$9, 30, 3, 995);
    			attr_dev(div1, "class", "currentWeatherIcon svelte-1ync8gy");
    			add_location(div1, file$9, 29, 2, 959);
    			attr_dev(div2, "class", "currentWeatherDesc svelte-1ync8gy");
    			add_location(div2, file$9, 32, 2, 1121);
    			attr_dev(div3, "class", "date svelte-1ync8gy");
    			add_location(div3, file$9, 36, 2, 1214);
    			html_tag_4 = new HtmlTag(null);
    			attr_dev(div4, "class", "currentTemp svelte-1ync8gy");
    			add_location(div4, file$9, 39, 2, 1270);
    			html_tag_5 = new HtmlTag(null);
    			attr_dev(div5, "class", "feelsLikeTemp svelte-1ync8gy");
    			add_location(div5, file$9, 42, 2, 1409);
    			attr_dev(div6, "class", "mainInformation svelte-1ync8gy");
    			add_location(div6, file$9, 23, 1, 626);
    			attr_dev(div7, "class", "hourlyTemps svelte-1ync8gy");
    			attr_dev(div7, "id", "hourlyTemps");
    			add_location(div7, file$9, 47, 1, 1576);
    			attr_dev(main, "class", "svelte-1ync8gy");
    			add_location(main, file$9, 14, 0, 420);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);
    			if (if_block) if_block.m(main, null);
    			append_dev(main, t0);
    			append_dev(main, div6);
    			append_dev(div6, div0);
    			append_dev(div0, t1);
    			append_dev(div0, t2);
    			html_tag.m(raw0_value, div0);
    			append_dev(div0, html_anchor);
    			html_tag_1.m(upArrow$1, div0);
    			append_dev(div0, t3);
    			append_dev(div0, t4);
    			html_tag_2.m(raw2_value, div0);
    			append_dev(div0, html_anchor_1);
    			html_tag_3.m(downArrow$1, div0);
    			append_dev(div6, t5);
    			append_dev(div6, div1);
    			append_dev(div1, img);
    			append_dev(div6, t6);
    			append_dev(div6, div2);
    			append_dev(div2, t7);
    			append_dev(div6, t8);
    			append_dev(div6, div3);
    			append_dev(div6, t10);
    			append_dev(div6, div4);
    			append_dev(div4, t11);
    			html_tag_4.m(raw4_value, div4);
    			append_dev(div6, t12);
    			append_dev(div6, div5);
    			append_dev(div5, t13);
    			append_dev(div5, t14);
    			html_tag_5.m(raw5_value, div5);
    			append_dev(main, t15);
    			append_dev(main, div7);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div7, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*weatherData*/ ctx[0].alerts) {
    				if (if_block) {
    					if_block.p(ctx, dirty);

    					if (dirty & /*weatherData*/ 1) {
    						transition_in(if_block, 1);
    					}
    				} else {
    					if_block = create_if_block_1$2(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(main, t0);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}

    			if ((!current || dirty & /*weatherData, $selectedTempUnit*/ 3) && t2_value !== (t2_value = convertTemp(/*weatherData*/ ctx[0].daily[0].temp.max, /*$selectedTempUnit*/ ctx[1]) + "")) set_data_dev(t2, t2_value);
    			if ((!current || dirty & /*$selectedTempUnit*/ 2) && raw0_value !== (raw0_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "")) html_tag.p(raw0_value);
    			if ((!current || dirty & /*weatherData, $selectedTempUnit*/ 3) && t4_value !== (t4_value = convertTemp(/*weatherData*/ ctx[0].daily[0].temp.min, /*$selectedTempUnit*/ ctx[1]) + "")) set_data_dev(t4, t4_value);
    			if ((!current || dirty & /*$selectedTempUnit*/ 2) && raw2_value !== (raw2_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "")) html_tag_2.p(raw2_value);

    			if (!current || dirty & /*weatherData*/ 1 && img.src !== (img_src_value = "images/" + /*weatherData*/ ctx[0].current.weather[0].icon + "@2x.png")) {
    				attr_dev(img, "src", img_src_value);
    			}

    			if (!current || dirty & /*weatherData*/ 1 && img_alt_value !== (img_alt_value = /*weatherData*/ ctx[0].current.weather[0].description)) {
    				attr_dev(img, "alt", img_alt_value);
    			}

    			if ((!current || dirty & /*weatherData*/ 1) && t7_value !== (t7_value = /*weatherData*/ ctx[0].current.weather[0].description + "")) set_data_dev(t7, t7_value);
    			if ((!current || dirty & /*weatherData, $selectedTempUnit*/ 3) && t11_value !== (t11_value = convertTemp(/*weatherData*/ ctx[0].current.temp, /*$selectedTempUnit*/ ctx[1]) + "")) set_data_dev(t11, t11_value);
    			if ((!current || dirty & /*$selectedTempUnit*/ 2) && raw4_value !== (raw4_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "")) html_tag_4.p(raw4_value);
    			if ((!current || dirty & /*weatherData, $selectedTempUnit*/ 3) && t14_value !== (t14_value = convertTemp(/*weatherData*/ ctx[0].current.feels_like, /*$selectedTempUnit*/ ctx[1]) + "")) set_data_dev(t14, t14_value);
    			if ((!current || dirty & /*$selectedTempUnit*/ 2) && raw5_value !== (raw5_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "")) html_tag_5.p(raw5_value);

    			if (dirty & /*timeConverter, weatherData, getTempSymbol, $selectedTempUnit, convertTemp*/ 3) {
    				each_value = /*weatherData*/ ctx[0].hourly.slice(0, 25);
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$2(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$2(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div7, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			if (if_block) if_block.d();
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$9.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    const upArrow$1 = "&#129045;";
    const downArrow$1 = "&#129047;";

    function timeConverter$1(unixTime) {
    	let date = new Date(unixTime * 1000);
    	return date.getHours();
    }

    function instance$9($$self, $$props, $$invalidate) {
    	let $selectedTempUnit;
    	validate_store(selectedTempUnit, "selectedTempUnit");
    	component_subscribe($$self, selectedTempUnit, $$value => $$invalidate(1, $selectedTempUnit = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("TodayForecast", slots, []);
    	
    	let { weatherData } = $$props;
    	console.log(weatherData);
    	let today = new Date();
    	const writable_props = ["weatherData"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1.warn(`<TodayForecast> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ("weatherData" in $$props) $$invalidate(0, weatherData = $$props.weatherData);
    	};

    	$$self.$capture_state = () => ({
    		selectedTempUnit,
    		convertTemp,
    		getTempSymbol,
    		WeatherWarning,
    		upArrow: upArrow$1,
    		downArrow: downArrow$1,
    		weatherData,
    		today,
    		timeConverter: timeConverter$1,
    		$selectedTempUnit
    	});

    	$$self.$inject_state = $$props => {
    		if ("weatherData" in $$props) $$invalidate(0, weatherData = $$props.weatherData);
    		if ("today" in $$props) $$invalidate(2, today = $$props.today);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [weatherData, $selectedTempUnit, today];
    }

    class TodayForecast extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$9, create_fragment$9, safe_not_equal, { weatherData: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "TodayForecast",
    			options,
    			id: create_fragment$9.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*weatherData*/ ctx[0] === undefined && !("weatherData" in props)) {
    			console_1.warn("<TodayForecast> was created without expected prop 'weatherData'");
    		}
    	}

    	get weatherData() {
    		throw new Error("<TodayForecast>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set weatherData(value) {
    		throw new Error("<TodayForecast>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/forecasts/TomorrowForecast.svelte generated by Svelte v3.38.2 */
    const file$8 = "src/components/forecasts/TomorrowForecast.svelte";

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[4] = list[i];
    	return child_ctx;
    }

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[7] = list[i];
    	return child_ctx;
    }

    // (25:1) {#if weatherData.alerts}
    function create_if_block_1$1(ctx) {
    	let div;
    	let current;
    	let each_value_1 = /*weatherData*/ ctx[0].alerts;
    	validate_each_argument(each_value_1);
    	let each_blocks = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks[i] = create_each_block_1(get_each_context_1(ctx, each_value_1, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const block = {
    		c: function create() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(div, "class", "weatherWarning");
    			add_location(div, file$8, 25, 2, 774);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*weatherData, checkEndTime*/ 9) {
    				each_value_1 = /*weatherData*/ ctx[0].alerts;
    				validate_each_argument(each_value_1);
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1(ctx, each_value_1, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block_1(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();

    				for (i = each_value_1.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value_1.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1$1.name,
    		type: "if",
    		source: "(25:1) {#if weatherData.alerts}",
    		ctx
    	});

    	return block;
    }

    // (28:4) {#if checkEndTime(alert.end)}
    function create_if_block_2$1(ctx) {
    	let weatherwarning;
    	let current;

    	weatherwarning = new WeatherWarning({
    			props: { alert: /*alert*/ ctx[7] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(weatherwarning.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(weatherwarning, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const weatherwarning_changes = {};
    			if (dirty & /*weatherData*/ 1) weatherwarning_changes.alert = /*alert*/ ctx[7];
    			weatherwarning.$set(weatherwarning_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(weatherwarning.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(weatherwarning.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(weatherwarning, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2$1.name,
    		type: "if",
    		source: "(28:4) {#if checkEndTime(alert.end)}",
    		ctx
    	});

    	return block;
    }

    // (27:3) {#each weatherData.alerts as alert}
    function create_each_block_1(ctx) {
    	let show_if = /*checkEndTime*/ ctx[3](/*alert*/ ctx[7].end);
    	let if_block_anchor;
    	let current;
    	let if_block = show_if && create_if_block_2$1(ctx);

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*weatherData*/ 1) show_if = /*checkEndTime*/ ctx[3](/*alert*/ ctx[7].end);

    			if (show_if) {
    				if (if_block) {
    					if_block.p(ctx, dirty);

    					if (dirty & /*weatherData*/ 1) {
    						transition_in(if_block, 1);
    					}
    				} else {
    					if_block = create_if_block_2$1(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_1.name,
    		type: "each",
    		source: "(27:3) {#each weatherData.alerts as alert}",
    		ctx
    	});

    	return block;
    }

    // (70:5) {:else}
    function create_else_block(ctx) {
    	let t0_value = timeConverter(/*hour*/ ctx[4].dt) - 12 + "";
    	let t0;
    	let t1;

    	const block = {
    		c: function create() {
    			t0 = text(t0_value);
    			t1 = text("p.m.");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t0, anchor);
    			insert_dev(target, t1, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*weatherData*/ 1 && t0_value !== (t0_value = timeConverter(/*hour*/ ctx[4].dt) - 12 + "")) set_data_dev(t0, t0_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(t1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(70:5) {:else}",
    		ctx
    	});

    	return block;
    }

    // (68:5) {#if timeConverter(hour.dt) <= 12}
    function create_if_block$3(ctx) {
    	let t0_value = timeConverter(/*hour*/ ctx[4].dt) + "";
    	let t0;
    	let t1;

    	const block = {
    		c: function create() {
    			t0 = text(t0_value);
    			t1 = text("a.m.");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t0, anchor);
    			insert_dev(target, t1, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*weatherData*/ 1 && t0_value !== (t0_value = timeConverter(/*hour*/ ctx[4].dt) + "")) set_data_dev(t0, t0_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(t1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$3.name,
    		type: "if",
    		source: "(68:5) {#if timeConverter(hour.dt) <= 12}",
    		ctx
    	});

    	return block;
    }

    // (57:2) {#each weatherData.hourly.slice(((tomorrow.getHours() + (24 - tomorrow.getHours())) - tomorrow.getHours()), 49 - tomorrow.getHours()) as hour}
    function create_each_block$1(ctx) {
    	let div3;
    	let div0;
    	let t0_value = convertTemp(/*hour*/ ctx[4].temp, /*$selectedTempUnit*/ ctx[1]) + "";
    	let t0;
    	let html_tag;
    	let raw_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "";
    	let t1;
    	let div1;
    	let img;
    	let img_src_value;
    	let img_alt_value;
    	let t2;
    	let div2;
    	let show_if;
    	let t3;

    	function select_block_type(ctx, dirty) {
    		if (show_if == null || dirty & /*weatherData*/ 1) show_if = !!(timeConverter(/*hour*/ ctx[4].dt) <= 12);
    		if (show_if) return create_if_block$3;
    		return create_else_block;
    	}

    	let current_block_type = select_block_type(ctx, -1);
    	let if_block = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div1 = element("div");
    			img = element("img");
    			t2 = space();
    			div2 = element("div");
    			if_block.c();
    			t3 = space();
    			html_tag = new HtmlTag(null);
    			add_location(div0, file$8, 58, 4, 2109);
    			if (img.src !== (img_src_value = "images/" + /*hour*/ ctx[4].weather[0].icon + ".png")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", img_alt_value = /*hour*/ ctx[4].weather[0].description);
    			add_location(img, file$8, 63, 5, 2231);
    			add_location(div1, file$8, 62, 4, 2220);
    			add_location(div2, file$8, 66, 4, 2329);
    			attr_dev(div3, "class", "hourlyTemp svelte-fq7q26");
    			add_location(div3, file$8, 57, 3, 2080);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div0);
    			append_dev(div0, t0);
    			html_tag.m(raw_value, div0);
    			append_dev(div3, t1);
    			append_dev(div3, div1);
    			append_dev(div1, img);
    			append_dev(div3, t2);
    			append_dev(div3, div2);
    			if_block.m(div2, null);
    			append_dev(div3, t3);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*weatherData, $selectedTempUnit*/ 3 && t0_value !== (t0_value = convertTemp(/*hour*/ ctx[4].temp, /*$selectedTempUnit*/ ctx[1]) + "")) set_data_dev(t0, t0_value);
    			if (dirty & /*$selectedTempUnit*/ 2 && raw_value !== (raw_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "")) html_tag.p(raw_value);

    			if (dirty & /*weatherData*/ 1 && img.src !== (img_src_value = "images/" + /*hour*/ ctx[4].weather[0].icon + ".png")) {
    				attr_dev(img, "src", img_src_value);
    			}

    			if (dirty & /*weatherData*/ 1 && img_alt_value !== (img_alt_value = /*hour*/ ctx[4].weather[0].description)) {
    				attr_dev(img, "alt", img_alt_value);
    			}

    			if (current_block_type === (current_block_type = select_block_type(ctx, dirty)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div2, null);
    				}
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);
    			if_block.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$1.name,
    		type: "each",
    		source: "(57:2) {#each weatherData.hourly.slice(((tomorrow.getHours() + (24 - tomorrow.getHours())) - tomorrow.getHours()), 49 - tomorrow.getHours()) as hour}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$8(ctx) {
    	let main;
    	let t0;
    	let div6;
    	let div0;
    	let t1;
    	let t2_value = convertTemp(/*weatherData*/ ctx[0].daily[1].temp.max, /*$selectedTempUnit*/ ctx[1]) + "";
    	let t2;
    	let html_tag;
    	let raw0_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "";
    	let html_anchor;
    	let html_tag_1;
    	let t3;
    	let t4_value = convertTemp(/*weatherData*/ ctx[0].daily[1].temp.min, /*$selectedTempUnit*/ ctx[1]) + "";
    	let t4;
    	let html_tag_2;
    	let raw2_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "";
    	let html_anchor_1;
    	let html_tag_3;
    	let t5;
    	let div1;
    	let img;
    	let img_src_value;
    	let img_alt_value;
    	let t6;
    	let div2;
    	let t7_value = /*weatherData*/ ctx[0].daily[1].weather[0].description + "";
    	let t7;
    	let t8;
    	let div3;
    	let t10;
    	let div4;
    	let t11_value = convertTemp(/*weatherData*/ ctx[0].daily[1].temp.day, /*$selectedTempUnit*/ ctx[1]) + "";
    	let t11;
    	let html_tag_4;
    	let raw4_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "";
    	let t12;
    	let div5;
    	let t13;
    	let t14_value = convertTemp(/*weatherData*/ ctx[0].daily[1].feels_like.day, /*$selectedTempUnit*/ ctx[1]) + "";
    	let t14;
    	let html_tag_5;
    	let raw5_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "";
    	let t15;
    	let div7;
    	let current;
    	let if_block = /*weatherData*/ ctx[0].alerts && create_if_block_1$1(ctx);
    	let each_value = /*weatherData*/ ctx[0].hourly.slice(/*tomorrow*/ ctx[2].getHours() + (24 - /*tomorrow*/ ctx[2].getHours()) - /*tomorrow*/ ctx[2].getHours(), 49 - /*tomorrow*/ ctx[2].getHours());
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			main = element("main");
    			if (if_block) if_block.c();
    			t0 = space();
    			div6 = element("div");
    			div0 = element("div");
    			t1 = text("Day ");
    			t2 = text(t2_value);
    			html_anchor = empty();
    			t3 = text(" Night ");
    			t4 = text(t4_value);
    			html_anchor_1 = empty();
    			t5 = space();
    			div1 = element("div");
    			img = element("img");
    			t6 = space();
    			div2 = element("div");
    			t7 = text(t7_value);
    			t8 = space();
    			div3 = element("div");
    			div3.textContent = `${/*tomorrow*/ ctx[2].toDateString()}`;
    			t10 = space();
    			div4 = element("div");
    			t11 = text(t11_value);
    			t12 = space();
    			div5 = element("div");
    			t13 = text("Will feel like: ");
    			t14 = text(t14_value);
    			t15 = space();
    			div7 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			html_tag = new HtmlTag(html_anchor);
    			html_tag_1 = new HtmlTag(t3);
    			html_tag_2 = new HtmlTag(html_anchor_1);
    			html_tag_3 = new HtmlTag(null);
    			attr_dev(div0, "class", "weatherMinMax svelte-fq7q26");
    			add_location(div0, file$8, 34, 2, 978);
    			if (img.src !== (img_src_value = "images/" + /*weatherData*/ ctx[0].daily[1].weather[0].icon + "@2x.png")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", img_alt_value = /*weatherData*/ ctx[0].current.weather[0].description);
    			add_location(img, file$8, 38, 3, 1306);
    			attr_dev(div1, "class", "currentWeatherIcon svelte-fq7q26");
    			add_location(div1, file$8, 37, 2, 1270);
    			attr_dev(div2, "class", "currentWeatherDesc svelte-fq7q26");
    			add_location(div2, file$8, 40, 2, 1433);
    			attr_dev(div3, "class", "date svelte-fq7q26");
    			add_location(div3, file$8, 44, 2, 1527);
    			html_tag_4 = new HtmlTag(null);
    			attr_dev(div4, "class", "currentTemp svelte-fq7q26");
    			add_location(div4, file$8, 47, 2, 1586);
    			html_tag_5 = new HtmlTag(null);
    			attr_dev(div5, "class", "feelsLikeTemp svelte-fq7q26");
    			add_location(div5, file$8, 50, 2, 1730);
    			attr_dev(div6, "class", "mainInformation svelte-fq7q26");
    			add_location(div6, file$8, 33, 1, 946);
    			attr_dev(div7, "class", "hourlyTemps svelte-fq7q26");
    			add_location(div7, file$8, 55, 1, 1906);
    			attr_dev(main, "class", "svelte-fq7q26");
    			add_location(main, file$8, 23, 0, 739);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);
    			if (if_block) if_block.m(main, null);
    			append_dev(main, t0);
    			append_dev(main, div6);
    			append_dev(div6, div0);
    			append_dev(div0, t1);
    			append_dev(div0, t2);
    			html_tag.m(raw0_value, div0);
    			append_dev(div0, html_anchor);
    			html_tag_1.m(upArrow, div0);
    			append_dev(div0, t3);
    			append_dev(div0, t4);
    			html_tag_2.m(raw2_value, div0);
    			append_dev(div0, html_anchor_1);
    			html_tag_3.m(downArrow, div0);
    			append_dev(div6, t5);
    			append_dev(div6, div1);
    			append_dev(div1, img);
    			append_dev(div6, t6);
    			append_dev(div6, div2);
    			append_dev(div2, t7);
    			append_dev(div6, t8);
    			append_dev(div6, div3);
    			append_dev(div6, t10);
    			append_dev(div6, div4);
    			append_dev(div4, t11);
    			html_tag_4.m(raw4_value, div4);
    			append_dev(div6, t12);
    			append_dev(div6, div5);
    			append_dev(div5, t13);
    			append_dev(div5, t14);
    			html_tag_5.m(raw5_value, div5);
    			append_dev(main, t15);
    			append_dev(main, div7);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div7, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*weatherData*/ ctx[0].alerts) {
    				if (if_block) {
    					if_block.p(ctx, dirty);

    					if (dirty & /*weatherData*/ 1) {
    						transition_in(if_block, 1);
    					}
    				} else {
    					if_block = create_if_block_1$1(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(main, t0);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}

    			if ((!current || dirty & /*weatherData, $selectedTempUnit*/ 3) && t2_value !== (t2_value = convertTemp(/*weatherData*/ ctx[0].daily[1].temp.max, /*$selectedTempUnit*/ ctx[1]) + "")) set_data_dev(t2, t2_value);
    			if ((!current || dirty & /*$selectedTempUnit*/ 2) && raw0_value !== (raw0_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "")) html_tag.p(raw0_value);
    			if ((!current || dirty & /*weatherData, $selectedTempUnit*/ 3) && t4_value !== (t4_value = convertTemp(/*weatherData*/ ctx[0].daily[1].temp.min, /*$selectedTempUnit*/ ctx[1]) + "")) set_data_dev(t4, t4_value);
    			if ((!current || dirty & /*$selectedTempUnit*/ 2) && raw2_value !== (raw2_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "")) html_tag_2.p(raw2_value);

    			if (!current || dirty & /*weatherData*/ 1 && img.src !== (img_src_value = "images/" + /*weatherData*/ ctx[0].daily[1].weather[0].icon + "@2x.png")) {
    				attr_dev(img, "src", img_src_value);
    			}

    			if (!current || dirty & /*weatherData*/ 1 && img_alt_value !== (img_alt_value = /*weatherData*/ ctx[0].current.weather[0].description)) {
    				attr_dev(img, "alt", img_alt_value);
    			}

    			if ((!current || dirty & /*weatherData*/ 1) && t7_value !== (t7_value = /*weatherData*/ ctx[0].daily[1].weather[0].description + "")) set_data_dev(t7, t7_value);
    			if ((!current || dirty & /*weatherData, $selectedTempUnit*/ 3) && t11_value !== (t11_value = convertTemp(/*weatherData*/ ctx[0].daily[1].temp.day, /*$selectedTempUnit*/ ctx[1]) + "")) set_data_dev(t11, t11_value);
    			if ((!current || dirty & /*$selectedTempUnit*/ 2) && raw4_value !== (raw4_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "")) html_tag_4.p(raw4_value);
    			if ((!current || dirty & /*weatherData, $selectedTempUnit*/ 3) && t14_value !== (t14_value = convertTemp(/*weatherData*/ ctx[0].daily[1].feels_like.day, /*$selectedTempUnit*/ ctx[1]) + "")) set_data_dev(t14, t14_value);
    			if ((!current || dirty & /*$selectedTempUnit*/ 2) && raw5_value !== (raw5_value = getTempSymbol(/*$selectedTempUnit*/ ctx[1]) + "")) html_tag_5.p(raw5_value);

    			if (dirty & /*timeConverter, weatherData, tomorrow, getTempSymbol, $selectedTempUnit, convertTemp*/ 7) {
    				each_value = /*weatherData*/ ctx[0].hourly.slice(/*tomorrow*/ ctx[2].getHours() + (24 - /*tomorrow*/ ctx[2].getHours()) - /*tomorrow*/ ctx[2].getHours(), 49 - /*tomorrow*/ ctx[2].getHours());
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div7, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			if (if_block) if_block.d();
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$8.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    const upArrow = "&#129045;";
    const downArrow = "&#129047;";

    function timeConverter(unixTime) {
    	let date = new Date(unixTime * 1000);
    	return date.getHours();
    }

    function instance$8($$self, $$props, $$invalidate) {
    	let $selectedTempUnit;
    	validate_store(selectedTempUnit, "selectedTempUnit");
    	component_subscribe($$self, selectedTempUnit, $$value => $$invalidate(1, $selectedTempUnit = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("TomorrowForecast", slots, []);
    	
    	let { weatherData } = $$props;
    	let tomorrow = new Date();
    	tomorrow.setDate(tomorrow.getDate() + 1);

    	// TODO: CHECK THIS!!!!! check that a warning that persists across days is displayed
    	function checkEndTime(unixTimestamp) {
    		let endTime = new Date(unixTimestamp * 1000);

    		if (endTime.getDate() >= tomorrow.getDate()) {
    			return true;
    		}

    		return false;
    	}

    	const writable_props = ["weatherData"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<TomorrowForecast> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ("weatherData" in $$props) $$invalidate(0, weatherData = $$props.weatherData);
    	};

    	$$self.$capture_state = () => ({
    		selectedTempUnit,
    		convertTemp,
    		getTempSymbol,
    		WeatherWarning,
    		weatherData,
    		upArrow,
    		downArrow,
    		timeConverter,
    		tomorrow,
    		checkEndTime,
    		$selectedTempUnit
    	});

    	$$self.$inject_state = $$props => {
    		if ("weatherData" in $$props) $$invalidate(0, weatherData = $$props.weatherData);
    		if ("tomorrow" in $$props) $$invalidate(2, tomorrow = $$props.tomorrow);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [weatherData, $selectedTempUnit, tomorrow, checkEndTime];
    }

    class TomorrowForecast extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$8, create_fragment$8, safe_not_equal, { weatherData: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "TomorrowForecast",
    			options,
    			id: create_fragment$8.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*weatherData*/ ctx[0] === undefined && !("weatherData" in props)) {
    			console.warn("<TomorrowForecast> was created without expected prop 'weatherData'");
    		}
    	}

    	get weatherData() {
    		throw new Error("<TomorrowForecast>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set weatherData(value) {
    		throw new Error("<TomorrowForecast>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    function cubicOut(t) {
        const f = t - 1.0;
        return f * f * f + 1.0;
    }

    function fade(node, { delay = 0, duration = 400, easing = identity } = {}) {
        const o = +getComputedStyle(node).opacity;
        return {
            delay,
            duration,
            easing,
            css: t => `opacity: ${t * o}`
        };
    }
    function slide(node, { delay = 0, duration = 400, easing = cubicOut } = {}) {
        const style = getComputedStyle(node);
        const opacity = +style.opacity;
        const height = parseFloat(style.height);
        const padding_top = parseFloat(style.paddingTop);
        const padding_bottom = parseFloat(style.paddingBottom);
        const margin_top = parseFloat(style.marginTop);
        const margin_bottom = parseFloat(style.marginBottom);
        const border_top_width = parseFloat(style.borderTopWidth);
        const border_bottom_width = parseFloat(style.borderBottomWidth);
        return {
            delay,
            duration,
            easing,
            css: t => 'overflow: hidden;' +
                `opacity: ${Math.min(t * 20, 1) * opacity};` +
                `height: ${t * height}px;` +
                `padding-top: ${t * padding_top}px;` +
                `padding-bottom: ${t * padding_bottom}px;` +
                `margin-top: ${t * margin_top}px;` +
                `margin-bottom: ${t * margin_bottom}px;` +
                `border-top-width: ${t * border_top_width}px;` +
                `border-bottom-width: ${t * border_bottom_width}px;`
        };
    }

    /* src/components/forecasts/DailyForecast.svelte generated by Svelte v3.38.2 */
    const file$7 = "src/components/forecasts/DailyForecast.svelte";

    // (54:1) {#if visible}
    function create_if_block$2(ctx) {
    	let div8;
    	let div0;
    	let t1;
    	let div1;
    	let t3;
    	let div2;
    	let t5;
    	let div3;
    	let t6_value = convertSpeed(/*daily*/ ctx[0].wind_speed, /*$selectedSpeedUnit*/ ctx[4]) + "";
    	let t6;
    	let t7;
    	let t8_value = getSpeedSymbol(/*$selectedSpeedUnit*/ ctx[4]) + "";
    	let t8;
    	let t9;
    	let t10_value = /*getWindDirection*/ ctx[6](/*daily*/ ctx[0].wind_deg) + "";
    	let t10;
    	let t11;
    	let t12_value = /*daily*/ ctx[0].wind_deg + "";
    	let t12;
    	let html_tag;
    	let raw_value = "&#176;" + "";
    	let t13;
    	let div4;
    	let t14_value = /*daily*/ ctx[0].humidity + "";
    	let t14;
    	let t15;
    	let t16;
    	let div5;
    	let t17_value = getUVRating(/*daily*/ ctx[0].uvi) + "";
    	let t17;
    	let t18;
    	let t19_value = /*daily*/ ctx[0].uvi + "";
    	let t19;
    	let t20;
    	let div6;
    	let t22;
    	let div7;
    	let t23_value = /*daily*/ ctx[0].clouds + "";
    	let t23;
    	let t24;
    	let div8_intro;
    	let div8_outro;
    	let current;

    	const block = {
    		c: function create() {
    			div8 = element("div");
    			div0 = element("div");
    			div0.textContent = "Wind";
    			t1 = space();
    			div1 = element("div");
    			div1.textContent = "Humidity";
    			t3 = space();
    			div2 = element("div");
    			div2.textContent = "UV Index";
    			t5 = space();
    			div3 = element("div");
    			t6 = text(t6_value);
    			t7 = space();
    			t8 = text(t8_value);
    			t9 = text(",\n\t\t\t\t");
    			t10 = text(t10_value);
    			t11 = space();
    			t12 = text(t12_value);
    			t13 = space();
    			div4 = element("div");
    			t14 = text(t14_value);
    			t15 = text("%");
    			t16 = space();
    			div5 = element("div");
    			t17 = text(t17_value);
    			t18 = text(", ");
    			t19 = text(t19_value);
    			t20 = space();
    			div6 = element("div");
    			div6.textContent = "Cloud Coverage";
    			t22 = space();
    			div7 = element("div");
    			t23 = text(t23_value);
    			t24 = text("%");
    			attr_dev(div0, "class", "windTitle svelte-10lbhdr");
    			add_location(div0, file$7, 55, 3, 1539);
    			attr_dev(div1, "class", "humidityTitle svelte-10lbhdr");
    			add_location(div1, file$7, 56, 3, 1576);
    			attr_dev(div2, "class", "uvTitle svelte-10lbhdr");
    			add_location(div2, file$7, 57, 3, 1621);
    			html_tag = new HtmlTag(null);
    			attr_dev(div3, "class", "windData svelte-10lbhdr");
    			add_location(div3, file$7, 58, 3, 1660);
    			attr_dev(div4, "class", "humidityData svelte-10lbhdr");
    			add_location(div4, file$7, 63, 3, 1867);
    			attr_dev(div5, "class", "uvData svelte-10lbhdr");
    			add_location(div5, file$7, 66, 3, 1929);
    			attr_dev(div6, "class", "cloudCoverageTitle svelte-10lbhdr");
    			add_location(div6, file$7, 69, 3, 2005);
    			attr_dev(div7, "class", "cloudCoverageData svelte-10lbhdr");
    			add_location(div7, file$7, 70, 3, 2061);
    			attr_dev(div8, "class", "extraInfo svelte-10lbhdr");
    			add_location(div8, file$7, 54, 2, 1493);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div8, anchor);
    			append_dev(div8, div0);
    			append_dev(div8, t1);
    			append_dev(div8, div1);
    			append_dev(div8, t3);
    			append_dev(div8, div2);
    			append_dev(div8, t5);
    			append_dev(div8, div3);
    			append_dev(div3, t6);
    			append_dev(div3, t7);
    			append_dev(div3, t8);
    			append_dev(div3, t9);
    			append_dev(div3, t10);
    			append_dev(div3, t11);
    			append_dev(div3, t12);
    			html_tag.m(raw_value, div3);
    			append_dev(div8, t13);
    			append_dev(div8, div4);
    			append_dev(div4, t14);
    			append_dev(div4, t15);
    			append_dev(div8, t16);
    			append_dev(div8, div5);
    			append_dev(div5, t17);
    			append_dev(div5, t18);
    			append_dev(div5, t19);
    			append_dev(div8, t20);
    			append_dev(div8, div6);
    			append_dev(div8, t22);
    			append_dev(div8, div7);
    			append_dev(div7, t23);
    			append_dev(div7, t24);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if ((!current || dirty & /*daily, $selectedSpeedUnit*/ 17) && t6_value !== (t6_value = convertSpeed(/*daily*/ ctx[0].wind_speed, /*$selectedSpeedUnit*/ ctx[4]) + "")) set_data_dev(t6, t6_value);
    			if ((!current || dirty & /*$selectedSpeedUnit*/ 16) && t8_value !== (t8_value = getSpeedSymbol(/*$selectedSpeedUnit*/ ctx[4]) + "")) set_data_dev(t8, t8_value);
    			if ((!current || dirty & /*daily*/ 1) && t10_value !== (t10_value = /*getWindDirection*/ ctx[6](/*daily*/ ctx[0].wind_deg) + "")) set_data_dev(t10, t10_value);
    			if ((!current || dirty & /*daily*/ 1) && t12_value !== (t12_value = /*daily*/ ctx[0].wind_deg + "")) set_data_dev(t12, t12_value);
    			if ((!current || dirty & /*daily*/ 1) && t14_value !== (t14_value = /*daily*/ ctx[0].humidity + "")) set_data_dev(t14, t14_value);
    			if ((!current || dirty & /*daily*/ 1) && t17_value !== (t17_value = getUVRating(/*daily*/ ctx[0].uvi) + "")) set_data_dev(t17, t17_value);
    			if ((!current || dirty & /*daily*/ 1) && t19_value !== (t19_value = /*daily*/ ctx[0].uvi + "")) set_data_dev(t19, t19_value);
    			if ((!current || dirty & /*daily*/ 1) && t23_value !== (t23_value = /*daily*/ ctx[0].clouds + "")) set_data_dev(t23, t23_value);
    		},
    		i: function intro(local) {
    			if (current) return;

    			add_render_callback(() => {
    				if (div8_outro) div8_outro.end(1);
    				if (!div8_intro) div8_intro = create_in_transition(div8, slide, {});
    				div8_intro.start();
    			});

    			current = true;
    		},
    		o: function outro(local) {
    			if (div8_intro) div8_intro.invalidate();
    			div8_outro = create_out_transition(div8, slide, {});
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div8);
    			if (detaching && div8_outro) div8_outro.end();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$2.name,
    		type: "if",
    		source: "(54:1) {#if visible}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$7(ctx) {
    	let main;
    	let div5;
    	let div0;
    	let t0_value = /*date*/ ctx[1].toDateString() + "";
    	let t0;
    	let t1;
    	let div1;
    	let t2_value = /*daily*/ ctx[0].weather[0].description + "";
    	let t2;
    	let t3;
    	let div2;
    	let img;
    	let img_src_value;
    	let img_alt_value;
    	let t4;
    	let div3;
    	let t5_value = convertTemp(/*daily*/ ctx[0].temp.max, /*$selectedTempUnit*/ ctx[3]) + "";
    	let t5;
    	let html_tag;
    	let raw0_value = getTempSymbol(/*$selectedTempUnit*/ ctx[3]) + "";
    	let t6;
    	let div4;
    	let t7_value = convertTemp(/*daily*/ ctx[0].temp.min, /*$selectedTempUnit*/ ctx[3]) + "";
    	let t7;
    	let html_tag_1;
    	let raw1_value = getTempSymbol(/*$selectedTempUnit*/ ctx[3]) + "";
    	let t8;
    	let current;
    	let mounted;
    	let dispose;
    	let if_block = /*visible*/ ctx[2] && create_if_block$2(ctx);

    	const block = {
    		c: function create() {
    			main = element("main");
    			div5 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div1 = element("div");
    			t2 = text(t2_value);
    			t3 = space();
    			div2 = element("div");
    			img = element("img");
    			t4 = space();
    			div3 = element("div");
    			t5 = text(t5_value);
    			t6 = space();
    			div4 = element("div");
    			t7 = text(t7_value);
    			t8 = space();
    			if (if_block) if_block.c();
    			attr_dev(div0, "class", "date svelte-10lbhdr");
    			add_location(div0, file$7, 37, 2, 953);
    			attr_dev(div1, "class", "currentWeatherDesc svelte-10lbhdr");
    			add_location(div1, file$7, 40, 2, 1008);
    			if (img.src !== (img_src_value = "images/" + /*daily*/ ctx[0].weather[0].icon + "@2x.png")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", img_alt_value = /*daily*/ ctx[0].weather[0].description);
    			add_location(img, file$7, 44, 3, 1122);
    			attr_dev(div2, "class", "currentWeatherIcon svelte-10lbhdr");
    			add_location(div2, file$7, 43, 2, 1086);
    			html_tag = new HtmlTag(null);
    			attr_dev(div3, "class", "tempMax svelte-10lbhdr");
    			add_location(div3, file$7, 46, 2, 1220);
    			html_tag_1 = new HtmlTag(null);
    			attr_dev(div4, "class", "tempMin svelte-10lbhdr");
    			add_location(div4, file$7, 49, 2, 1345);
    			attr_dev(div5, "class", "mainCard svelte-10lbhdr");
    			add_location(div5, file$7, 36, 1, 905);
    			add_location(main, file$7, 35, 0, 897);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);
    			append_dev(main, div5);
    			append_dev(div5, div0);
    			append_dev(div0, t0);
    			append_dev(div5, t1);
    			append_dev(div5, div1);
    			append_dev(div1, t2);
    			append_dev(div5, t3);
    			append_dev(div5, div2);
    			append_dev(div2, img);
    			append_dev(div5, t4);
    			append_dev(div5, div3);
    			append_dev(div3, t5);
    			html_tag.m(raw0_value, div3);
    			append_dev(div5, t6);
    			append_dev(div5, div4);
    			append_dev(div4, t7);
    			html_tag_1.m(raw1_value, div4);
    			append_dev(main, t8);
    			if (if_block) if_block.m(main, null);
    			current = true;

    			if (!mounted) {
    				dispose = listen_dev(div5, "click", /*handleClick*/ ctx[5], false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if ((!current || dirty & /*date*/ 2) && t0_value !== (t0_value = /*date*/ ctx[1].toDateString() + "")) set_data_dev(t0, t0_value);
    			if ((!current || dirty & /*daily*/ 1) && t2_value !== (t2_value = /*daily*/ ctx[0].weather[0].description + "")) set_data_dev(t2, t2_value);

    			if (!current || dirty & /*daily*/ 1 && img.src !== (img_src_value = "images/" + /*daily*/ ctx[0].weather[0].icon + "@2x.png")) {
    				attr_dev(img, "src", img_src_value);
    			}

    			if (!current || dirty & /*daily*/ 1 && img_alt_value !== (img_alt_value = /*daily*/ ctx[0].weather[0].description)) {
    				attr_dev(img, "alt", img_alt_value);
    			}

    			if ((!current || dirty & /*daily, $selectedTempUnit*/ 9) && t5_value !== (t5_value = convertTemp(/*daily*/ ctx[0].temp.max, /*$selectedTempUnit*/ ctx[3]) + "")) set_data_dev(t5, t5_value);
    			if ((!current || dirty & /*$selectedTempUnit*/ 8) && raw0_value !== (raw0_value = getTempSymbol(/*$selectedTempUnit*/ ctx[3]) + "")) html_tag.p(raw0_value);
    			if ((!current || dirty & /*daily, $selectedTempUnit*/ 9) && t7_value !== (t7_value = convertTemp(/*daily*/ ctx[0].temp.min, /*$selectedTempUnit*/ ctx[3]) + "")) set_data_dev(t7, t7_value);
    			if ((!current || dirty & /*$selectedTempUnit*/ 8) && raw1_value !== (raw1_value = getTempSymbol(/*$selectedTempUnit*/ ctx[3]) + "")) html_tag_1.p(raw1_value);

    			if (/*visible*/ ctx[2]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);

    					if (dirty & /*visible*/ 4) {
    						transition_in(if_block, 1);
    					}
    				} else {
    					if_block = create_if_block$2(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(main, null);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			if (if_block) if_block.d();
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$7.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function getUVRating(index) {
    	index = Math.floor(index);
    	let result;

    	if (index <= 2) {
    		result = "Low";
    	} else if (index <= 5) {
    		result = "Moderate";
    	} else if (index <= 7) {
    		result = "High";
    	} else if (index <= 10) {
    		result = "Very High";
    	} else {
    		result = "Extreme";
    	}

    	return result;
    }

    function instance$7($$self, $$props, $$invalidate) {
    	let $selectedTempUnit;
    	let $selectedSpeedUnit;
    	validate_store(selectedTempUnit, "selectedTempUnit");
    	component_subscribe($$self, selectedTempUnit, $$value => $$invalidate(3, $selectedTempUnit = $$value));
    	validate_store(selectedSpeedUnit, "selectedSpeedUnit");
    	component_subscribe($$self, selectedSpeedUnit, $$value => $$invalidate(4, $selectedSpeedUnit = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("DailyForecast", slots, []);
    	
    	let { daily } = $$props;
    	let { date } = $$props;
    	let visible = false;

    	let directions = [
    		"N",
    		"NE",
    		"NE",
    		"E",
    		"E",
    		"SE",
    		"SE",
    		"S",
    		"S",
    		"SW",
    		"SW",
    		"W",
    		"W",
    		"NW",
    		"NW",
    		"N"
    	];

    	function handleClick() {
    		$$invalidate(2, visible = !visible);
    	}

    	function getWindDirection(degree) {
    		return directions[Math.floor(degree / 22.5)];
    	}

    	const writable_props = ["daily", "date"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<DailyForecast> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ("daily" in $$props) $$invalidate(0, daily = $$props.daily);
    		if ("date" in $$props) $$invalidate(1, date = $$props.date);
    	};

    	$$self.$capture_state = () => ({
    		selectedTempUnit,
    		selectedSpeedUnit,
    		convertTemp,
    		convertSpeed,
    		getTempSymbol,
    		getSpeedSymbol,
    		slide,
    		daily,
    		date,
    		visible,
    		directions,
    		handleClick,
    		getUVRating,
    		getWindDirection,
    		$selectedTempUnit,
    		$selectedSpeedUnit
    	});

    	$$self.$inject_state = $$props => {
    		if ("daily" in $$props) $$invalidate(0, daily = $$props.daily);
    		if ("date" in $$props) $$invalidate(1, date = $$props.date);
    		if ("visible" in $$props) $$invalidate(2, visible = $$props.visible);
    		if ("directions" in $$props) directions = $$props.directions;
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		daily,
    		date,
    		visible,
    		$selectedTempUnit,
    		$selectedSpeedUnit,
    		handleClick,
    		getWindDirection
    	];
    }

    class DailyForecast extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$7, create_fragment$7, safe_not_equal, { daily: 0, date: 1 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "DailyForecast",
    			options,
    			id: create_fragment$7.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*daily*/ ctx[0] === undefined && !("daily" in props)) {
    			console.warn("<DailyForecast> was created without expected prop 'daily'");
    		}

    		if (/*date*/ ctx[1] === undefined && !("date" in props)) {
    			console.warn("<DailyForecast> was created without expected prop 'date'");
    		}
    	}

    	get daily() {
    		throw new Error("<DailyForecast>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set daily(value) {
    		throw new Error("<DailyForecast>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get date() {
    		throw new Error("<DailyForecast>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set date(value) {
    		throw new Error("<DailyForecast>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/forecasts/SevenDayForecast.svelte generated by Svelte v3.38.2 */
    const file$6 = "src/components/forecasts/SevenDayForecast.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[3] = list[i];
    	child_ctx[5] = i;
    	return child_ctx;
    }

    // (15:1) {#each weatherData.daily as day, index}
    function create_each_block(ctx) {
    	let div;
    	let dailyforecast;
    	let t;
    	let current;

    	dailyforecast = new DailyForecast({
    			props: {
    				daily: /*day*/ ctx[3],
    				date: /*nextDay*/ ctx[1](/*index*/ ctx[5])
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(dailyforecast.$$.fragment);
    			t = space();
    			attr_dev(div, "class", "day svelte-cle336");
    			add_location(div, file$6, 15, 2, 338);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(dailyforecast, div, null);
    			append_dev(div, t);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const dailyforecast_changes = {};
    			if (dirty & /*weatherData*/ 1) dailyforecast_changes.daily = /*day*/ ctx[3];
    			dailyforecast.$set(dailyforecast_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(dailyforecast.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(dailyforecast.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(dailyforecast);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(15:1) {#each weatherData.daily as day, index}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$6(ctx) {
    	let main;
    	let current;
    	let each_value = /*weatherData*/ ctx[0].daily;
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const block = {
    		c: function create() {
    			main = element("main");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(main, "class", "svelte-cle336");
    			add_location(main, file$6, 13, 0, 288);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(main, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*weatherData, nextDay*/ 3) {
    				each_value = /*weatherData*/ ctx[0].daily;
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(main, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$6.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$6($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("SevenDayForecast", slots, []);
    	
    	let { weatherData } = $$props;
    	let date = new Date();

    	//date.setDate(date.getDate() + 1);
    	function nextDay(index) {
    		if (index > 0) {
    			date.setDate(date.getDate() + 1);
    		}

    		return date;
    	}

    	const writable_props = ["weatherData"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<SevenDayForecast> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ("weatherData" in $$props) $$invalidate(0, weatherData = $$props.weatherData);
    	};

    	$$self.$capture_state = () => ({
    		DailyForecast,
    		weatherData,
    		date,
    		nextDay
    	});

    	$$self.$inject_state = $$props => {
    		if ("weatherData" in $$props) $$invalidate(0, weatherData = $$props.weatherData);
    		if ("date" in $$props) date = $$props.date;
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [weatherData, nextDay];
    }

    class SevenDayForecast extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$6, create_fragment$6, safe_not_equal, { weatherData: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "SevenDayForecast",
    			options,
    			id: create_fragment$6.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*weatherData*/ ctx[0] === undefined && !("weatherData" in props)) {
    			console.warn("<SevenDayForecast> was created without expected prop 'weatherData'");
    		}
    	}

    	get weatherData() {
    		throw new Error("<SevenDayForecast>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set weatherData(value) {
    		throw new Error("<SevenDayForecast>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/Modal.svelte generated by Svelte v3.38.2 */
    const file$5 = "src/components/Modal.svelte";
    const get_footer_slot_changes = dirty => ({});
    const get_footer_slot_context = ctx => ({ store: /*store*/ ctx[1] });
    const get_content_slot_changes = dirty => ({});
    const get_content_slot_context = ctx => ({ store: /*store*/ ctx[1] });
    const get_header_slot_changes = dirty => ({});
    const get_header_slot_context = ctx => ({ store: /*store*/ ctx[1] });
    const get_trigger_slot_changes = dirty => ({});
    const get_trigger_slot_context = ctx => ({ open: /*open*/ ctx[3] });

    // (50:29)    
    function fallback_block_2(ctx) {
    	let button;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			button = element("button");
    			button.textContent = "Open";
    			add_location(button, file$5, 51, 2, 1425);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, button, anchor);

    			if (!mounted) {
    				dispose = listen_dev(button, "click", /*open*/ ctx[3], false, false, false);
    				mounted = true;
    			}
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(button);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: fallback_block_2.name,
    		type: "fallback",
    		source: "(50:29)    ",
    		ctx
    	});

    	return block;
    }

    // (54:1) {#if $isOpen}
    function create_if_block$1(ctx) {
    	let div3;
    	let div0;
    	let t0;
    	let div2;
    	let t1;
    	let div1;
    	let t2;
    	let current;
    	let mounted;
    	let dispose;
    	const header_slot_template = /*#slots*/ ctx[7].header;
    	const header_slot = create_slot(header_slot_template, ctx, /*$$scope*/ ctx[6], get_header_slot_context);
    	const header_slot_or_fallback = header_slot || fallback_block_1(ctx);
    	const content_slot_template = /*#slots*/ ctx[7].content;
    	const content_slot = create_slot(content_slot_template, ctx, /*$$scope*/ ctx[6], get_content_slot_context);
    	const footer_slot_template = /*#slots*/ ctx[7].footer;
    	const footer_slot = create_slot(footer_slot_template, ctx, /*$$scope*/ ctx[6], get_footer_slot_context);
    	const footer_slot_or_fallback = footer_slot || fallback_block(ctx);

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			div0 = element("div");
    			t0 = space();
    			div2 = element("div");
    			if (header_slot_or_fallback) header_slot_or_fallback.c();
    			t1 = space();
    			div1 = element("div");
    			if (content_slot) content_slot.c();
    			t2 = space();
    			if (footer_slot_or_fallback) footer_slot_or_fallback.c();
    			attr_dev(div0, "class", "backdrop svelte-1k66zvv");
    			add_location(div0, file$5, 55, 3, 1541);
    			attr_dev(div1, "class", "content svelte-1k66zvv");
    			add_location(div1, file$5, 65, 4, 1760);
    			attr_dev(div2, "class", "content-wrapper svelte-1k66zvv");
    			add_location(div2, file$5, 57, 3, 1587);
    			attr_dev(div3, "class", "modal svelte-1k66zvv");
    			attr_dev(div3, "tabindex", "0");
    			add_location(div3, file$5, 54, 2, 1489);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div0);
    			append_dev(div3, t0);
    			append_dev(div3, div2);

    			if (header_slot_or_fallback) {
    				header_slot_or_fallback.m(div2, null);
    			}

    			append_dev(div2, t1);
    			append_dev(div2, div1);

    			if (content_slot) {
    				content_slot.m(div1, null);
    			}

    			append_dev(div2, t2);

    			if (footer_slot_or_fallback) {
    				footer_slot_or_fallback.m(div2, null);
    			}

    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(div0, "click", /*close*/ ctx[4], false, false, false),
    					action_destroyer(/*modalAction*/ ctx[5].call(null, div3))
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (header_slot) {
    				if (header_slot.p && (!current || dirty & /*$$scope*/ 64)) {
    					update_slot(header_slot, header_slot_template, ctx, /*$$scope*/ ctx[6], dirty, get_header_slot_changes, get_header_slot_context);
    				}
    			}

    			if (content_slot) {
    				if (content_slot.p && (!current || dirty & /*$$scope*/ 64)) {
    					update_slot(content_slot, content_slot_template, ctx, /*$$scope*/ ctx[6], dirty, get_content_slot_changes, get_content_slot_context);
    				}
    			}

    			if (footer_slot) {
    				if (footer_slot.p && (!current || dirty & /*$$scope*/ 64)) {
    					update_slot(footer_slot, footer_slot_template, ctx, /*$$scope*/ ctx[6], dirty, get_footer_slot_changes, get_footer_slot_context);
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(header_slot_or_fallback, local);
    			transition_in(content_slot, local);
    			transition_in(footer_slot_or_fallback, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(header_slot_or_fallback, local);
    			transition_out(content_slot, local);
    			transition_out(footer_slot_or_fallback, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);
    			if (header_slot_or_fallback) header_slot_or_fallback.d(detaching);
    			if (content_slot) content_slot.d(detaching);
    			if (footer_slot_or_fallback) footer_slot_or_fallback.d(detaching);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$1.name,
    		type: "if",
    		source: "(54:1) {#if $isOpen}",
    		ctx
    	});

    	return block;
    }

    // (59:32)       
    function fallback_block_1(ctx) {
    	let div;
    	let h1;

    	const block = {
    		c: function create() {
    			div = element("div");
    			h1 = element("h1");
    			h1.textContent = "Your Modal Heading Goes Here...";
    			attr_dev(h1, "class", "svelte-1k66zvv");
    			add_location(h1, file$5, 61, 6, 1690);
    			add_location(div, file$5, 60, 5, 1678);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h1);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: fallback_block_1.name,
    		type: "fallback",
    		source: "(59:32)       ",
    		ctx
    	});

    	return block;
    }

    // (70:32)       
    function fallback_block(ctx) {
    	let div;
    	let h1;
    	let t1;
    	let button;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			h1 = element("h1");
    			h1.textContent = "Your Modal Footer Goes Here...";
    			t1 = space();
    			button = element("button");
    			button.textContent = "Close";
    			attr_dev(h1, "class", "svelte-1k66zvv");
    			add_location(h1, file$5, 72, 6, 1904);
    			add_location(button, file$5, 73, 6, 1950);
    			add_location(div, file$5, 71, 5, 1892);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h1);
    			append_dev(div, t1);
    			append_dev(div, button);

    			if (!mounted) {
    				dispose = listen_dev(button, "click", /*close*/ ctx[4], false, false, false);
    				mounted = true;
    			}
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: fallback_block.name,
    		type: "fallback",
    		source: "(70:32)       ",
    		ctx
    	});

    	return block;
    }

    function create_fragment$5(ctx) {
    	let main;
    	let t;
    	let current;
    	const trigger_slot_template = /*#slots*/ ctx[7].trigger;
    	const trigger_slot = create_slot(trigger_slot_template, ctx, /*$$scope*/ ctx[6], get_trigger_slot_context);
    	const trigger_slot_or_fallback = trigger_slot || fallback_block_2(ctx);
    	let if_block = /*$isOpen*/ ctx[0] && create_if_block$1(ctx);

    	const block = {
    		c: function create() {
    			main = element("main");
    			if (trigger_slot_or_fallback) trigger_slot_or_fallback.c();
    			t = space();
    			if (if_block) if_block.c();
    			add_location(main, file$5, 48, 0, 1340);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);

    			if (trigger_slot_or_fallback) {
    				trigger_slot_or_fallback.m(main, null);
    			}

    			append_dev(main, t);
    			if (if_block) if_block.m(main, null);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (trigger_slot) {
    				if (trigger_slot.p && (!current || dirty & /*$$scope*/ 64)) {
    					update_slot(trigger_slot, trigger_slot_template, ctx, /*$$scope*/ ctx[6], dirty, get_trigger_slot_changes, get_trigger_slot_context);
    				}
    			}

    			if (/*$isOpen*/ ctx[0]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);

    					if (dirty & /*$isOpen*/ 1) {
    						transition_in(if_block, 1);
    					}
    				} else {
    					if_block = create_if_block$1(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(main, null);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(trigger_slot_or_fallback, local);
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(trigger_slot_or_fallback, local);
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			if (trigger_slot_or_fallback) trigger_slot_or_fallback.d(detaching);
    			if (if_block) if_block.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$5.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    const modalList = [];

    function transitionend(e) {
    	const node = e.target;
    	node.focus();
    }

    function instance$5($$self, $$props, $$invalidate) {
    	let $isOpen;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("Modal", slots, ['trigger','header','content','footer']);
    	const store = booleanStore(false);
    	const { isOpen, open, close } = store;
    	validate_store(isOpen, "isOpen");
    	component_subscribe($$self, isOpen, value => $$invalidate(0, $isOpen = value));

    	function keydown(e) {
    		e.stopPropagation();

    		if (e.key === "Escape") {
    			close();
    		}
    	}

    	function modalAction(node) {
    		const returnFn = [];

    		// for accessibility
    		if (document.body.style.overflow !== "hidden") {
    			const original = document.body.style.overflow;
    			document.body.style.overflow = "hidden";

    			returnFn.push(() => {
    				document.body.style.overflow = original;
    			});
    		}

    		node.addEventListener("keydown", keydown);
    		node.addEventListener("transitionend", transitionend);
    		node.focus();
    		modalList.push(node);

    		returnFn.push(() => {
    			node.removeEventListener("keydown", keydown);
    			node.removeEventListener("transitionend", transitionend);
    			modalList.pop();

    			// Optional chaining to guard against empty array.
    			modalList[modalList.length - 1]?.focus();
    		});

    		return {
    			destroy: () => returnFn.forEach(fn => fn())
    		};
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Modal> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ("$$scope" in $$props) $$invalidate(6, $$scope = $$props.$$scope);
    	};

    	$$self.$capture_state = () => ({
    		modalList,
    		booleanStore,
    		store,
    		isOpen,
    		open,
    		close,
    		keydown,
    		transitionend,
    		modalAction,
    		$isOpen
    	});

    	return [$isOpen, store, isOpen, open, close, modalAction, $$scope, slots];
    }

    class Modal extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Modal",
    			options,
    			id: create_fragment$5.name
    		});
    	}
    }

    /* src/components/OptionsDialog.svelte generated by Svelte v3.38.2 */
    const file$4 = "src/components/OptionsDialog.svelte";

    function create_fragment$4(ctx) {
    	let main;
    	let h30;
    	let t1;
    	let label0;
    	let input0;
    	let t2;
    	let t3;
    	let label1;
    	let input1;
    	let t4;
    	let t5;
    	let label2;
    	let input2;
    	let t6;
    	let t7;
    	let h31;
    	let t9;
    	let label3;
    	let input3;
    	let t10;
    	let t11;
    	let label4;
    	let input4;
    	let t12;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			main = element("main");
    			h30 = element("h3");
    			h30.textContent = "Preferred Temperature Unit";
    			t1 = space();
    			label0 = element("label");
    			input0 = element("input");
    			t2 = text("\n\t\tKelvin");
    			t3 = space();
    			label1 = element("label");
    			input1 = element("input");
    			t4 = text("\n\t\tCelsius");
    			t5 = space();
    			label2 = element("label");
    			input2 = element("input");
    			t6 = text("\n\t\tFahrenheit");
    			t7 = space();
    			h31 = element("h3");
    			h31.textContent = "Preferred Speed Unit";
    			t9 = space();
    			label3 = element("label");
    			input3 = element("input");
    			t10 = text("\n\t\tKpH");
    			t11 = space();
    			label4 = element("label");
    			input4 = element("input");
    			t12 = text("\n\t\tMpH");
    			attr_dev(h30, "class", "svelte-1gpgyj4");
    			add_location(h30, file$4, 6, 1, 158);
    			attr_dev(input0, "type", "radio");
    			input0.__value = TempUnit.Kelvin;
    			input0.value = input0.__value;
    			/*$$binding_groups*/ ctx[3][0].push(input0);
    			add_location(input0, file$4, 8, 2, 205);
    			add_location(label0, file$4, 7, 1, 195);
    			attr_dev(input1, "type", "radio");
    			input1.__value = TempUnit.Celsius;
    			input1.value = input1.__value;
    			/*$$binding_groups*/ ctx[3][0].push(input1);
    			add_location(input1, file$4, 13, 2, 314);
    			add_location(label1, file$4, 12, 1, 304);
    			attr_dev(input2, "type", "radio");
    			input2.__value = TempUnit.Fahrenheit;
    			input2.value = input2.__value;
    			/*$$binding_groups*/ ctx[3][0].push(input2);
    			add_location(input2, file$4, 18, 2, 425);
    			add_location(label2, file$4, 17, 1, 415);
    			attr_dev(h31, "class", "svelte-1gpgyj4");
    			add_location(h31, file$4, 22, 1, 532);
    			attr_dev(input3, "type", "radio");
    			input3.__value = SpeedUnit.KpH;
    			input3.value = input3.__value;
    			/*$$binding_groups*/ ctx[3][1].push(input3);
    			add_location(input3, file$4, 24, 2, 573);
    			add_location(label3, file$4, 23, 1, 563);
    			attr_dev(input4, "type", "radio");
    			input4.__value = SpeedUnit.MpH;
    			input4.value = input4.__value;
    			/*$$binding_groups*/ ctx[3][1].push(input4);
    			add_location(input4, file$4, 29, 2, 678);
    			add_location(label4, file$4, 28, 1, 668);
    			add_location(main, file$4, 5, 0, 150);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, main, anchor);
    			append_dev(main, h30);
    			append_dev(main, t1);
    			append_dev(main, label0);
    			append_dev(label0, input0);
    			input0.checked = input0.__value === /*$selectedTempUnit*/ ctx[0];
    			append_dev(label0, t2);
    			append_dev(main, t3);
    			append_dev(main, label1);
    			append_dev(label1, input1);
    			input1.checked = input1.__value === /*$selectedTempUnit*/ ctx[0];
    			append_dev(label1, t4);
    			append_dev(main, t5);
    			append_dev(main, label2);
    			append_dev(label2, input2);
    			input2.checked = input2.__value === /*$selectedTempUnit*/ ctx[0];
    			append_dev(label2, t6);
    			append_dev(main, t7);
    			append_dev(main, h31);
    			append_dev(main, t9);
    			append_dev(main, label3);
    			append_dev(label3, input3);
    			input3.checked = input3.__value === /*$selectedSpeedUnit*/ ctx[1];
    			append_dev(label3, t10);
    			append_dev(main, t11);
    			append_dev(main, label4);
    			append_dev(label4, input4);
    			input4.checked = input4.__value === /*$selectedSpeedUnit*/ ctx[1];
    			append_dev(label4, t12);

    			if (!mounted) {
    				dispose = [
    					listen_dev(input0, "change", /*input0_change_handler*/ ctx[2]),
    					listen_dev(input1, "change", /*input1_change_handler*/ ctx[4]),
    					listen_dev(input2, "change", /*input2_change_handler*/ ctx[5]),
    					listen_dev(input3, "change", /*input3_change_handler*/ ctx[6]),
    					listen_dev(input4, "change", /*input4_change_handler*/ ctx[7])
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*$selectedTempUnit*/ 1) {
    				input0.checked = input0.__value === /*$selectedTempUnit*/ ctx[0];
    			}

    			if (dirty & /*$selectedTempUnit*/ 1) {
    				input1.checked = input1.__value === /*$selectedTempUnit*/ ctx[0];
    			}

    			if (dirty & /*$selectedTempUnit*/ 1) {
    				input2.checked = input2.__value === /*$selectedTempUnit*/ ctx[0];
    			}

    			if (dirty & /*$selectedSpeedUnit*/ 2) {
    				input3.checked = input3.__value === /*$selectedSpeedUnit*/ ctx[1];
    			}

    			if (dirty & /*$selectedSpeedUnit*/ 2) {
    				input4.checked = input4.__value === /*$selectedSpeedUnit*/ ctx[1];
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(main);
    			/*$$binding_groups*/ ctx[3][0].splice(/*$$binding_groups*/ ctx[3][0].indexOf(input0), 1);
    			/*$$binding_groups*/ ctx[3][0].splice(/*$$binding_groups*/ ctx[3][0].indexOf(input1), 1);
    			/*$$binding_groups*/ ctx[3][0].splice(/*$$binding_groups*/ ctx[3][0].indexOf(input2), 1);
    			/*$$binding_groups*/ ctx[3][1].splice(/*$$binding_groups*/ ctx[3][1].indexOf(input3), 1);
    			/*$$binding_groups*/ ctx[3][1].splice(/*$$binding_groups*/ ctx[3][1].indexOf(input4), 1);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let $selectedTempUnit;
    	let $selectedSpeedUnit;
    	validate_store(selectedTempUnit, "selectedTempUnit");
    	component_subscribe($$self, selectedTempUnit, $$value => $$invalidate(0, $selectedTempUnit = $$value));
    	validate_store(selectedSpeedUnit, "selectedSpeedUnit");
    	component_subscribe($$self, selectedSpeedUnit, $$value => $$invalidate(1, $selectedSpeedUnit = $$value));
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("OptionsDialog", slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<OptionsDialog> was created with unknown prop '${key}'`);
    	});

    	const $$binding_groups = [[], []];

    	function input0_change_handler() {
    		$selectedTempUnit = this.__value;
    		selectedTempUnit.set($selectedTempUnit);
    	}

    	function input1_change_handler() {
    		$selectedTempUnit = this.__value;
    		selectedTempUnit.set($selectedTempUnit);
    	}

    	function input2_change_handler() {
    		$selectedTempUnit = this.__value;
    		selectedTempUnit.set($selectedTempUnit);
    	}

    	function input3_change_handler() {
    		$selectedSpeedUnit = this.__value;
    		selectedSpeedUnit.set($selectedSpeedUnit);
    	}

    	function input4_change_handler() {
    		$selectedSpeedUnit = this.__value;
    		selectedSpeedUnit.set($selectedSpeedUnit);
    	}

    	$$self.$capture_state = () => ({
    		selectedTempUnit,
    		selectedSpeedUnit,
    		TempUnit,
    		SpeedUnit,
    		$selectedTempUnit,
    		$selectedSpeedUnit
    	});

    	return [
    		$selectedTempUnit,
    		$selectedSpeedUnit,
    		input0_change_handler,
    		$$binding_groups,
    		input1_change_handler,
    		input2_change_handler,
    		input3_change_handler,
    		input4_change_handler
    	];
    }

    class OptionsDialog extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "OptionsDialog",
    			options,
    			id: create_fragment$4.name
    		});
    	}
    }

    /* src/components/Header.svelte generated by Svelte v3.38.2 */
    const file$3 = "src/components/Header.svelte";

    // (11:3) 
    function create_trigger_slot(ctx) {
    	let div;
    	let button;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			button = element("button");
    			button.textContent = "Options";
    			attr_dev(button, "class", "optionsButton");
    			add_location(button, file$3, 11, 4, 300);
    			attr_dev(div, "slot", "trigger");
    			add_location(div, file$3, 10, 3, 266);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, button);

    			if (!mounted) {
    				dispose = listen_dev(
    					button,
    					"click",
    					function () {
    						if (is_function(/*open*/ ctx[1])) /*open*/ ctx[1].apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				);

    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_trigger_slot.name,
    		type: "slot",
    		source: "(11:3) ",
    		ctx
    	});

    	return block;
    }

    // (14:3) 
    function create_header_slot(ctx) {
    	let div;
    	let h1;

    	const block = {
    		c: function create() {
    			div = element("div");
    			h1 = element("h1");
    			h1.textContent = "Options";
    			add_location(h1, file$3, 14, 4, 400);
    			attr_dev(div, "slot", "header");
    			add_location(div, file$3, 13, 3, 376);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, h1);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_header_slot.name,
    		type: "slot",
    		source: "(14:3) ",
    		ctx
    	});

    	return block;
    }

    // (17:3) 
    function create_content_slot(ctx) {
    	let div;
    	let optionsdialog;
    	let current;
    	optionsdialog = new OptionsDialog({ $$inline: true });

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(optionsdialog.$$.fragment);
    			attr_dev(div, "slot", "content");
    			add_location(div, file$3, 16, 3, 430);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(optionsdialog, div, null);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(optionsdialog.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(optionsdialog.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(optionsdialog);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_content_slot.name,
    		type: "slot",
    		source: "(17:3) ",
    		ctx
    	});

    	return block;
    }

    // (20:3) 
    function create_footer_slot(ctx) {
    	let div;
    	let button;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			button = element("button");
    			button.textContent = "Close";
    			add_location(button, file$3, 20, 4, 532);
    			attr_dev(div, "slot", "footer");
    			add_location(div, file$3, 19, 3, 486);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, button);

    			if (!mounted) {
    				dispose = listen_dev(
    					button,
    					"click",
    					function () {
    						if (is_function(/*close*/ ctx[0])) /*close*/ ctx[0].apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				);

    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_footer_slot.name,
    		type: "slot",
    		source: "(20:3) ",
    		ctx
    	});

    	return block;
    }

    function create_fragment$3(ctx) {
    	let header;
    	let img;
    	let img_src_value;
    	let t0;
    	let h1;
    	let t2;
    	let div;
    	let modal;
    	let current;

    	modal = new Modal({
    			props: {
    				$$slots: {
    					footer: [
    						create_footer_slot,
    						({ store: { close } }) => ({ 0: close }),
    						({ store: close_close }) => close_close ? 1 : 0
    					],
    					content: [create_content_slot],
    					header: [create_header_slot],
    					trigger: [
    						create_trigger_slot,
    						({ open }) => ({ 1: open }),
    						({ open }) => open ? 2 : 0
    					]
    				},
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			header = element("header");
    			img = element("img");
    			t0 = space();
    			h1 = element("h1");
    			h1.textContent = "Local Weather Forecast";
    			t2 = space();
    			div = element("div");
    			create_component(modal.$$.fragment);
    			if (img.src !== (img_src_value = "images/weathericon.jpeg")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", "Weather App Logo");
    			attr_dev(img, "class", "svelte-14wvwsy");
    			add_location(img, file$3, 5, 1, 135);
    			add_location(h1, file$3, 6, 1, 197);
    			attr_dev(div, "class", "options svelte-14wvwsy");
    			add_location(div, file$3, 8, 1, 231);
    			attr_dev(header, "class", "svelte-14wvwsy");
    			add_location(header, file$3, 4, 0, 125);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, header, anchor);
    			append_dev(header, img);
    			append_dev(header, t0);
    			append_dev(header, h1);
    			append_dev(header, t2);
    			append_dev(header, div);
    			mount_component(modal, div, null);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			const modal_changes = {};

    			if (dirty & /*$$scope, close, open*/ 7) {
    				modal_changes.$$scope = { dirty, ctx };
    			}

    			modal.$set(modal_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(modal.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(modal.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(header);
    			destroy_component(modal);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("Header", slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Header> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({ Modal, OptionsDialog });
    	return [];
    }

    class Header extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Header",
    			options,
    			id: create_fragment$3.name
    		});
    	}
    }

    /* src/components/Footer.svelte generated by Svelte v3.38.2 */

    const file$2 = "src/components/Footer.svelte";

    function create_fragment$2(ctx) {
    	let footer;
    	let div;

    	const block = {
    		c: function create() {
    			footer = element("footer");
    			div = element("div");
    			div.textContent = "Weather Forecast Website built using Svelte by Alex Tompkins";
    			attr_dev(div, "class", "footer svelte-zd8gsx");
    			add_location(div, file$2, 1, 4, 13);
    			attr_dev(footer, "class", "svelte-zd8gsx");
    			add_location(footer, file$2, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, footer, anchor);
    			append_dev(footer, div);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(footer);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$2($$self, $$props) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("Footer", slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Footer> was created with unknown prop '${key}'`);
    	});

    	return [];
    }

    class Footer extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Footer",
    			options,
    			id: create_fragment$2.name
    		});
    	}
    }

    /* node_modules/svelte-loading-spinners/dist/Circle.svelte generated by Svelte v3.38.2 */

    const file$1 = "node_modules/svelte-loading-spinners/dist/Circle.svelte";

    function create_fragment$1(ctx) {
    	let div;

    	const block = {
    		c: function create() {
    			div = element("div");
    			attr_dev(div, "class", "circle svelte-14upwad");
    			set_style(div, "--size", /*size*/ ctx[3] + /*unit*/ ctx[1]);
    			set_style(div, "--color", /*color*/ ctx[0]);
    			set_style(div, "--duration", /*duration*/ ctx[2]);
    			add_location(div, file$1, 28, 0, 626);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*size, unit*/ 10) {
    				set_style(div, "--size", /*size*/ ctx[3] + /*unit*/ ctx[1]);
    			}

    			if (dirty & /*color*/ 1) {
    				set_style(div, "--color", /*color*/ ctx[0]);
    			}

    			if (dirty & /*duration*/ 4) {
    				set_style(div, "--duration", /*duration*/ ctx[2]);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("Circle", slots, []);
    	
    	let { color = "#FF3E00" } = $$props;
    	let { unit = "px" } = $$props;
    	let { duration = "0.75s" } = $$props;
    	let { size = "60" } = $$props;
    	const writable_props = ["color", "unit", "duration", "size"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Circle> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ("color" in $$props) $$invalidate(0, color = $$props.color);
    		if ("unit" in $$props) $$invalidate(1, unit = $$props.unit);
    		if ("duration" in $$props) $$invalidate(2, duration = $$props.duration);
    		if ("size" in $$props) $$invalidate(3, size = $$props.size);
    	};

    	$$self.$capture_state = () => ({ color, unit, duration, size });

    	$$self.$inject_state = $$props => {
    		if ("color" in $$props) $$invalidate(0, color = $$props.color);
    		if ("unit" in $$props) $$invalidate(1, unit = $$props.unit);
    		if ("duration" in $$props) $$invalidate(2, duration = $$props.duration);
    		if ("size" in $$props) $$invalidate(3, size = $$props.size);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [color, unit, duration, size];
    }

    class Circle extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, { color: 0, unit: 1, duration: 2, size: 3 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Circle",
    			options,
    			id: create_fragment$1.name
    		});
    	}

    	get color() {
    		throw new Error("<Circle>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set color(value) {
    		throw new Error("<Circle>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get unit() {
    		throw new Error("<Circle>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set unit(value) {
    		throw new Error("<Circle>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get duration() {
    		throw new Error("<Circle>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set duration(value) {
    		throw new Error("<Circle>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get size() {
    		throw new Error("<Circle>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set size(value) {
    		throw new Error("<Circle>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/App.svelte generated by Svelte v3.38.2 */
    const file = "src/App.svelte";

    // (1:0) <script lang="typescript">var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {     function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }
    function create_catch_block(ctx) {
    	const block = {
    		c: noop,
    		m: noop,
    		p: noop,
    		i: noop,
    		o: noop,
    		d: noop
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_catch_block.name,
    		type: "catch",
    		source: "(1:0) <script lang=\\\"typescript\\\">var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {     function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }",
    		ctx
    	});

    	return block;
    }

    // (65:1) {:then data}
    function create_then_block(ctx) {
    	let current_block_type_index;
    	let if_block;
    	let if_block_anchor;
    	let current;
    	const if_block_creators = [create_if_block, create_if_block_1, create_if_block_2];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*activeItem*/ ctx[0] === "Today") return 0;
    		if (/*activeItem*/ ctx[0] === "Tomorrow") return 1;
    		if (/*activeItem*/ ctx[0] === "7 Days") return 2;
    		return -1;
    	}

    	if (~(current_block_type_index = select_block_type(ctx))) {
    		if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    	}

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			if (~current_block_type_index) {
    				if_blocks[current_block_type_index].m(target, anchor);
    			}

    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if (~current_block_type_index) {
    					if_blocks[current_block_type_index].p(ctx, dirty);
    				}
    			} else {
    				if (if_block) {
    					group_outros();

    					transition_out(if_blocks[previous_block_index], 1, 1, () => {
    						if_blocks[previous_block_index] = null;
    					});

    					check_outros();
    				}

    				if (~current_block_type_index) {
    					if_block = if_blocks[current_block_type_index];

    					if (!if_block) {
    						if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    						if_block.c();
    					} else {
    						if_block.p(ctx, dirty);
    					}

    					transition_in(if_block, 1);
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				} else {
    					if_block = null;
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (~current_block_type_index) {
    				if_blocks[current_block_type_index].d(detaching);
    			}

    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_then_block.name,
    		type: "then",
    		source: "(65:1) {:then data}",
    		ctx
    	});

    	return block;
    }

    // (74:36) 
    function create_if_block_2(ctx) {
    	let div;
    	let sevendayforecast;
    	let div_intro;
    	let current;

    	sevendayforecast = new SevenDayForecast({
    			props: { weatherData: /*data*/ ctx[7] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(sevendayforecast.$$.fragment);
    			add_location(div, file, 74, 3, 2885);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(sevendayforecast, div, null);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(sevendayforecast.$$.fragment, local);

    			if (!div_intro) {
    				add_render_callback(() => {
    					div_intro = create_in_transition(div, fade, {});
    					div_intro.start();
    				});
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(sevendayforecast.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(sevendayforecast);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2.name,
    		type: "if",
    		source: "(74:36) ",
    		ctx
    	});

    	return block;
    }

    // (70:38) 
    function create_if_block_1(ctx) {
    	let div;
    	let tomorrowforecast;
    	let div_intro;
    	let current;

    	tomorrowforecast = new TomorrowForecast({
    			props: { weatherData: /*data*/ ctx[7] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(tomorrowforecast.$$.fragment);
    			add_location(div, file, 70, 3, 2777);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(tomorrowforecast, div, null);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(tomorrowforecast.$$.fragment, local);

    			if (!div_intro) {
    				add_render_callback(() => {
    					div_intro = create_in_transition(div, fade, {});
    					div_intro.start();
    				});
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(tomorrowforecast.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(tomorrowforecast);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(70:38) ",
    		ctx
    	});

    	return block;
    }

    // (66:2) {#if activeItem === 'Today'}
    function create_if_block(ctx) {
    	let div;
    	let todayforecast;
    	let div_intro;
    	let current;

    	todayforecast = new TodayForecast({
    			props: { weatherData: /*data*/ ctx[7] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(todayforecast.$$.fragment);
    			add_location(div, file, 66, 3, 2670);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(todayforecast, div, null);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(todayforecast.$$.fragment, local);

    			if (!div_intro) {
    				add_render_callback(() => {
    					div_intro = create_in_transition(div, fade, {});
    					div_intro.start();
    				});
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(todayforecast.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(todayforecast);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(66:2) {#if activeItem === 'Today'}",
    		ctx
    	});

    	return block;
    }

    // (61:21)    <div class="loading">    <Circle color={'#d91b42'}
    function create_pending_block(ctx) {
    	let div;
    	let circle;
    	let current;

    	circle = new Circle({
    			props: { color: "#d91b42" },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(circle.$$.fragment);
    			attr_dev(div, "class", "loading svelte-km2g0k");
    			add_location(div, file, 61, 2, 2559);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(circle, div, null);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(circle.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(circle.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(circle);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_pending_block.name,
    		type: "pending",
    		source: "(61:21)    <div class=\\\"loading\\\">    <Circle color={'#d91b42'}",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let header;
    	let t0;
    	let main;
    	let tabs;
    	let t1;
    	let t2;
    	let footer;
    	let current;
    	header = new Header({ $$inline: true });

    	tabs = new Tabs({
    			props: {
    				activeItem: /*activeItem*/ ctx[0],
    				items: /*items*/ ctx[2]
    			},
    			$$inline: true
    		});

    	tabs.$on("tabChange", /*tabChange*/ ctx[3]);

    	let info = {
    		ctx,
    		current: null,
    		token: null,
    		hasCatch: false,
    		pending: create_pending_block,
    		then: create_then_block,
    		catch: create_catch_block,
    		value: 7,
    		blocks: [,,,]
    	};

    	handle_promise(/*weatherData*/ ctx[1], info);
    	footer = new Footer({ $$inline: true });

    	const block = {
    		c: function create() {
    			create_component(header.$$.fragment);
    			t0 = space();
    			main = element("main");
    			create_component(tabs.$$.fragment);
    			t1 = space();
    			info.block.c();
    			t2 = space();
    			create_component(footer.$$.fragment);
    			attr_dev(main, "class", "svelte-km2g0k");
    			add_location(main, file, 58, 0, 2472);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			mount_component(header, target, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, main, anchor);
    			mount_component(tabs, main, null);
    			append_dev(main, t1);
    			info.block.m(main, info.anchor = null);
    			info.mount = () => main;
    			info.anchor = null;
    			insert_dev(target, t2, anchor);
    			mount_component(footer, target, anchor);
    			current = true;
    		},
    		p: function update(new_ctx, [dirty]) {
    			ctx = new_ctx;
    			const tabs_changes = {};
    			if (dirty & /*activeItem*/ 1) tabs_changes.activeItem = /*activeItem*/ ctx[0];
    			tabs.$set(tabs_changes);
    			update_await_block_branch(info, ctx, dirty);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(header.$$.fragment, local);
    			transition_in(tabs.$$.fragment, local);
    			transition_in(info.block);
    			transition_in(footer.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(header.$$.fragment, local);
    			transition_out(tabs.$$.fragment, local);

    			for (let i = 0; i < 3; i += 1) {
    				const block = info.blocks[i];
    				transition_out(block);
    			}

    			transition_out(footer.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(header, detaching);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(main);
    			destroy_component(tabs);
    			info.block.d();
    			info.token = null;
    			info = null;
    			if (detaching) detach_dev(t2);
    			destroy_component(footer, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("App", slots, []);

    	var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
    		function adopt(value) {
    			return value instanceof P
    			? value
    			: new P(function (resolve) {
    						resolve(value);
    					});
    		}

    		return new (P || (P = Promise))(function (resolve, reject) {
    				function fulfilled(value) {
    					try {
    						step(generator.next(value));
    					} catch(e) {
    						reject(e);
    					}
    				}

    				function rejected(value) {
    					try {
    						step(generator["throw"](value));
    					} catch(e) {
    						reject(e);
    					}
    				}

    				function step(result) {
    					result.done
    					? resolve(result.value)
    					: adopt(result.value).then(fulfilled, rejected);
    				}

    				step((generator = generator.apply(thisArg, _arguments || [])).next());
    			});
    	};

    	
    	let weatherData = getData();

    	// tabs
    	let items = ["Today", "Tomorrow", "7 Days"];

    	let activeItem = "Today";

    	const tabChange = e => {
    		$$invalidate(0, activeItem = e.detail);
    	};

    	function getData() {
    		return __awaiter(this, void 0, void 0, function* () {
    			let position;

    			try {
    				position = yield getCoords();
    				changeCoords(position.coords.latitude, position.coords.longitude);
    			} catch(e) {
    				// Error getting user location (suer denied request / no internet)
    				window.alert("Error getting user location: Using Nicola, BC as default.");
    			} // if (e instanceof GeolocationPositionError) {
    			// 		console.log('Access Denied!');

    			// 	} else if (e.code == 2 || e.code == 3) {
    			// 		console.log('No Internet!');
    			// 	}
    			// }
    			let response = yield fetch(oneCallURL);

    			return yield response.json();
    		});
    	}

    	function getCoords() {
    		return __awaiter(this, void 0, void 0, function* () {
    			return new Promise(function (resolve, reject) {
    					navigator.geolocation.getCurrentPosition(resolve, reject);
    				});
    		});
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({
    		__awaiter,
    		Tabs,
    		oneCallURL,
    		changeCoords,
    		TodayForecast,
    		TomorrowForecast,
    		SevenDayForecast,
    		Header,
    		Footer,
    		fade,
    		Circle,
    		weatherData,
    		items,
    		activeItem,
    		tabChange,
    		getData,
    		getCoords
    	});

    	$$self.$inject_state = $$props => {
    		if ("__awaiter" in $$props) __awaiter = $$props.__awaiter;
    		if ("weatherData" in $$props) $$invalidate(1, weatherData = $$props.weatherData);
    		if ("items" in $$props) $$invalidate(2, items = $$props.items);
    		if ("activeItem" in $$props) $$invalidate(0, activeItem = $$props.activeItem);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [activeItem, weatherData, items, tabChange];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment.name
    		});
    	}
    }

    const app = new App({
        target: document.body
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
