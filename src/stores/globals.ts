import { writable } from 'svelte/store';
import { TempUnit, SpeedUnit } from './enums';

const apiKey: string = "fe9bfa061883b7ebbb0b034fa3305714";
const city: string = "Calgary";

const celsiusSymbol = "&#8451;";
const fahrenheitSymbol = "&#8457;";
const kelvinSymbol = "&#8490;";

const KpHSymbol = "km/h";
const MpHSymbol = "Mph";

// Calgary
//const callatitude: number = 51.1250295;
//const callongitude: number = -114.2507148;
//Nicola BC
// const latitude: number = 49.2580;
// const longitude: number = -122.7440;

let latitude = 49.2580;
let longitude = -122.7440;

export let currentURL: string = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${apiKey}`;
export let oneCallURL: string = `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&appid=${apiKey}`;

export const selectedTempUnit = writable(TempUnit.Celsius);
export const selectedSpeedUnit = writable(SpeedUnit.KpH);

export function changeCoords(newLatitude: number, newLongitude: number) {
    latitude = newLatitude;
    longitude = newLongitude;

    // don't like this: since not a .svelte file the latitude/longitude values can't be used reactively so have to mainly update...
    oneCallURL = `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&appid=${apiKey}`;
}

// default value from api is in Kelvin so value passed in will always be in Kelvin
export function convertTemp(value: number, unit: TempUnit): String {
    let result: number;
    switch (unit) {
        case TempUnit.Kelvin:
            result = value;
            break;
        case TempUnit.Celsius:
            result = value - 273.15;
            break;
        case TempUnit.Fahrenheit:
            result = (value - 273.15) * (9 / 5) + 32;
            break;
    }
    return result.toFixed(1);
}

// passed value is always in m/s
export function convertSpeed(value: number, unit: SpeedUnit): String {
    let result: number;
    switch(unit) {
        case SpeedUnit.KpH:
            result = value * 3.6;
            break;
        case SpeedUnit.MpH:
            result = value * 2.237;
            break;
    }

    return result.toFixed(0);
}

export function getTempSymbol(unit: TempUnit) {
    switch (unit) {
        case TempUnit.Kelvin:
            return kelvinSymbol;
        case TempUnit.Celsius:
            return celsiusSymbol;
        case TempUnit.Fahrenheit:
            return fahrenheitSymbol;
    }
}

export function getSpeedSymbol(unit: SpeedUnit) {
    switch (unit) {
        case SpeedUnit.KpH:
            return KpHSymbol;
        case SpeedUnit.MpH:
            return MpHSymbol;
    }
}

export function booleanStore(initial) {
    const isOpen = writable(initial)
    const { set, update } = isOpen
    return {
      isOpen,
      open: () => set(true),
      close: () => set(false),
      toggle: () => update((n) => !n),
    }
  }