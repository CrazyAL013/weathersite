export enum TempUnit {
    Kelvin,
    Celsius,
    Fahrenheit
}

export enum SpeedUnit {
    KpH,
    MpH
}